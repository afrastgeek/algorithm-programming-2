//program rekursif kelipatan
#include "header.h"

int main() {
    int kelipatan; //ini kelipatan
    int baris, kolom;
    
    scanf("%d", &kelipatan);
    scanf("%d %d", &baris, &kolom);
    
    int matrix[baris][kolom];
    
    int i, j;
    
    for(i = 0; i < baris; i += 1) {
        for(j = 0; j < kolom; j += 1) {
            scanf("%d", &matrix[i][j]);
        }
    }
    
    for (i = 0; i < baris; i += 1) {
        for (j = 0; j < kolom; j += 1) {
            matrix[i][j] = sumKelipatan(kelipatan, matrix[i][j]);
        }
    }

    for(i = 0; i < baris; i += 1) {
        for(j = 0; j < kolom; j += 1) {
            printf("%d", matrix[i][j]);
        }
        printf("\n");
    }
    return 0;
}
