#include "header.h"

int sumKelipatan(int kelipatan, int n) {

    if (n < kelipatan) {
        return 0;
    } else {
        int jumlah;
        jumlah = (n/kelipatan)*kelipatan;

        return jumlah + sumKelipatan(kelipatan, jumlah - kelipatan);
    }
}
