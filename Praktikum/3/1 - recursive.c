#include <stdio.h>

void funct()
{
    printf("\n This is a recursive function \n");
    funct();
    return;
}

int main()
{
    funct();
    return 0;
}