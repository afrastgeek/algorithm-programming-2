#include <stdio.h>

/**
 * @param matrix	matrix to search
 * @param x			value sought
 */
int findValue(int matrix[5][5], int x) {
	// assume row and column of the matrix are 5
	int row, column;

	// status variable / mark when value found
	int stats = 0;

	// loop as much as the row, then the column.
	row = 0;
	while (row < 5 && stats == 0) {
		column = 0;
		while (column < 5 && stats == 0) {
			if (matrix[row][column] == x) {
				// return true when value found
				stats = 1;
			} else {
				column += 1;
			}
		}
		row += 1;
	}

	return stats;
}

int main(int argc, char const *argv[])
{
	int matrix[5][5];
	int row, column;
	int x;

	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			scanf("%d", &matrix[row][column]);
		}
	}

	scanf("%d", &x);
	if (findValue(matrix, x)) {
		printf("there's equivalent value in the matrix\n");
	} else {
		printf("not found\n");
	}

	return 0;
}