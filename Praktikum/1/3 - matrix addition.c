#include <stdio.h>

int main(int argc, char const *argv[])
{
	int row, column;

	int matrix1[5][5];
	int matrix2[5][5];
	int matrix3[5][5];

	// insert value to the matrix 1
	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			scanf("%d", &matrix1[row][column]);
		}
	}

	// insert value to the matrix 2
	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			scanf("%d", &matrix2[row][column]);
		}
	}

	// sum up the matrix
	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			matrix3[row][column] = matrix1[row][column] + matrix2[row][column];
		}
	}

	// show it up
	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			printf("%d ", matrix3[row][column]);
		}
		printf("\n");
	}

	return 0;
}