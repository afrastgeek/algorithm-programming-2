/*
Program menukar tiap elemen yang dicari di dalam matriks.
Command program tsb adalah:
Replace n x y Cari letak x y, lalu ubah dengan angka n
Swap n x y Cari letak angka n, lalu tukar dengan angka
pada koordinat x,y
Contoh masukkan :
3 5
1 2 3 4 5
6 7 8 9 10
11 12 13 14 15
3
Replace 99 3 3
Swap 11 1 1
Swap 10 1 2
*/
#include <stdio.h>

typedef struct coordinates {
	int x;
	int y;
} coordinates;

/**
 * @param matrix	matrix to search
 * @param x			value sought
 */
coordinates getXY(int which_row, int which_column, int matrix[5][5], int x) {
	// assume row and column of the matrix are 5
	int row, column;
	coordinates value;

	// status variable / mark when value found
	int stats = 0;

	// loop as much as the row, then the column.
	row = 0;
	while (row < 5 && stats == 0) {
		column = 0;
		while (column < 5 && stats == 0) {
			if (matrix[row][column] == x) {
				// return true when value found
				stats = 1;
				value.x = column;
				value.y = row;
			} else {
				column += 1;
			}
		}
		row += 1;
	}

	return value;
}

int main(int argc, char const *argv[]) {
	int index, row, column;
	int row_num, column_num, command_num;
	int a, b, c;

	char command[8];

	scanf("%d %d", &row_num, &column_num);

	int matrix[row_num][column_num];
	for (row = 0; row < row_num; row += 1) {
		for (column = 0; column < column_num; column += 1) {
			scanf("%d", &matrix[row][column]);
		}
	}

	scanf("%d", &command_num);

	for (index = 0; index < command_num; index += 1) {
		scanf("%s %d %d %d", command, &a, &b, &c);

		if (strcmp(command, "Replace") == 0) {
			matrix[b - 1][c - 1] = a;
		}
	}

	return 0;
}