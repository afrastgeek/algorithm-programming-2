/*
Buatlah program yang dapat menukar tiap baris matriks.
Command program tersebuh adalah :
TukarB x y Tukar baris x dengan baris y
TukarK x y Tukar kolom x dengan kolom Y

Contoh masukkan :
3 5
1 2 3 4 5
6 7 8 9 10
11 12 13 14 15
3
TukarB 1 3
TukarK 2 5
TukarK 4 1
*/
#include <stdio.h>

int main(int argc, char const *argv[]) {
	int index, row, column;
	int row_num, column_num, command_num;
	int which_row, which_column;

	char command[8];

	scanf("%d %d", &row_num, &column_num);

	int matrix[row_num][column_num];
	for (row = 0; row < row_num; row += 1) {
		for (column = 0; column < column_num; column += 1) {
			scanf("%d", &matrix[row][column]);
		}
	}

	scanf("%d", &command_num);

	for (index = 0; index < command_num; index += 1) {
		scanf("%s %d %d", command, &which_row, &which_column);
		
	}

	return 0;
}