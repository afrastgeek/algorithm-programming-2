#include <stdio.h>

int main(int argc, char const *argv[])
{
	int matrix[5][5];
	int row, column;

	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 5; column += 1) {
			scanf("%d", &matrix[row][column]);
		}
	}
	
	return 0;
}