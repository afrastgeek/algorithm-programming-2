#include <stdio.h>

int main(int argc, char const *argv[])
{
	int row, column, count;

	int matrix1[4][5];
	int matrix2[5][6];
	int matrix3[4][6];

	// insert value to the matrix 1
	for (row = 0; row < 4; row += 1) {
		for (column = 0; column < 5; column += 1) {
			scanf("%d", &matrix1[row][column]);
		}
	}

	// insert value to the matrix 2
	for (row = 0; row < 5; row += 1) {
		for (column = 0; column < 6; column += 1) {
			scanf("%d", &matrix2[row][column]);
		}
	}

	// multiply the matrix
	for (row = 0; row < 4; row += 1) {
		for (column = 0; column < 6; column += 1) {
			matrix3[row][column] = 0;
			for (count = 0; count < 5; count += 1) {
				matrix3[row][column] += (matrix1[row][count] * matrix2[count][column]);
			}
		}
	}

	// show it up
	for (row = 0; row < 4; row += 1) {
		for (column = 0; column < 6; column += 1) {
			printf("%d ", matrix3[row][column]);
		}
		printf("\n");
	}

	return 0;
}