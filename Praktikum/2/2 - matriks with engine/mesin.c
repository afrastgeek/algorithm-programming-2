#include "header.h"

void cetak (int m, int n, koordinat titik[m][n]) {
	int i, j;
	
	for (i = 0; i < m; i += 1) {
		for (j = 0; j < n; j += 1) {
			printf("Elemen pada baris ke %d dan kolom ke %d\n", i, j);
			printf("Koordinat x : %d ", titik[i][j].x);
			printf("Koordinat y : %d\n", titik[i][j].y);
		}
	}
}