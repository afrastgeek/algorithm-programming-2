/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Latihan Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/


#include "header.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, and pattern display are executed.
 *
 * @param argc The parameter are additional. void also could be used.
 * @param argv The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an int
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const *argv[]) {
  /**
   * @brief Declaration Section
   */
  int i = 0, j;                   // variabel interasi
  int max_length[3] = {0, 0, 0};  // store max_length of each record
  int length;                     // total length of longest record
  int pilihan;                    // variabel pilihan proses
  char pilihan2[100];  // variabel untuk melanjutkan atau keluar program
  mahasiswa alpro;     // variabel bungkusan

  /**
   * @brief CRUD Menu Selection Section
   */
  do {
    /**
     * @brief Print The Data
     */
    system("cls");  // menghapus layar
    readFile();     // pemanggilan file yang sudah dibaca

    getMaxLength(max_length); /* call procedure to get the max_length of rec. */
    length = max_length[0] + max_length[1] + max_length[2] +
             10;  // 10 is border and it's one character space accumulated

    for (j = 0; j < length; j += 1) {
      printf("-"); /* top border */
    }
    printf("\n");

    printf("| NIM"); /* first row header */
    for (i = 0; i < max_length[0] - strlen("NIM"); i += 1) {
      printf(" "); /* spacing max to longest record - "NIM" */
    }
    printf(" | Nama"); /* first row header */
    for (i = 0; i < max_length[1] - strlen("Nama"); i += 1) {
      printf(" "); /* spacing max to longest record - "Nama" */
    }
    printf(" | Kelas "); /* first row header */
    for (i = 0; i < max_length[2] - strlen("Kelas"); i += 1) {
      printf(" "); /* spacing max to longest record - "Kelas" */
    }
    printf("|\n");

    for (j = 0; j < length; j += 1) {
      printf("-"); /* first row bottom border */
    }
    printf("\n");

    for (i = 0; i < n; i += 1) {
      printf("| %s", data[i].nim); /* print the NIM data */
      for (j = 0; j < max_length[0] - strlen(data[i].nim); ++j) {
        printf(" "); /* spacing max to longest record */
      }
      printf(" | %s", data[i].nama); /* print the Nama data */
      for (j = 0; j < max_length[1] - strlen(data[i].nama); ++j) {
        printf(" "); /* spacing max to longest record */
      }
      printf(" | %s", data[i].kelas); /* print the Kelas data */
      for (j = 0; j < max_length[2] - strlen(data[i].kelas); j += 1) {
        printf(" "); /* spacing max to longest record */
      }
      printf(" |\n");
    }

    for (j = 0; j < length; j += 1) {
      printf("-"); /* bottom border */
    }
    printf("\n\n");

    // proses penampilan pilihan proses
    printf("Silahkan Masukan Pilihan Anda!\n");
    printf("\n");
    printf("--------Pilihan--------\n");
    printf("|");
    for (i = 0; i < strlen(data[i].nama) - 3; i += 1) {
      printf(" ");
    }
    printf("1. Cari Data       |\n");  // untuk mencari data
    printf("|");
    for (i = 0; i < strlen(data[i].nama) - 3; i += 1) {
      printf(" ");
    }
    printf("2. Tambah Data     |\n");  // untuk menambah data
    printf("|");
    for (i = 0; i < strlen(data[i].nama) - 3; i += 1) {
      printf(" ");
    }
    printf("3. Update Data     |\n");  // untuk update data
    printf("|");
    for (i = 0; i < strlen(data[i].nama) - 3; i += 1) {
      printf(" ");
    }
    printf("4. Delete Data     |\n");  // untuk menghapus data
    printf("|");
    for (i = 0; i < strlen(data[i].nama) - 3; i += 1) {
      printf(" ");
    }
    printf("5. Exit            |\n");  // untuk keluar dari menu
    printf("-----------------------\n");
    printf("\n");

    // input pilihan proses
    scanf("%d", &pilihan);

    // jika pilihan no 1
    if (pilihan == 1) {
      // proses mencari data
      // input nim yang ingin dicari
      printf("Masukan NIM : ");
      scanf("%s", alpro.nim);
      // pemanggilan fungsi mencari
      if (findData(alpro.nim) == 1) {
        printf("Data Ditemukan\n");
      } else {
        // jika nim tidak ada
        printf("Data Tidak Ditemukan\n");
      }
    }

    // jika pilihan 2
    else if (pilihan == 2) {
      // proses menambah data baru
      printf("Masukan NIM : ");
      scanf("%s", alpro.nim);
      printf("Masukan Nama : ");
      scanf("%s", alpro.nama);
      printf("Masukan kelas : ");
      scanf("%s", alpro.kelas);
      // pemanggilan prosedur menambah data
      insertData(alpro);
    }

    // jika pilihan 3
    else if (pilihan == 3) {
      // proses mengupdate data
      // input nim yang akan diupdate
      printf("Masukan NIM : ");
      scanf("%s", alpro.nim);
      // pemanggilan prosedur update
      updateData(alpro.nim);
    }

    // jika pilihan 4
    else if (pilihan == 4) {
      // proses menghapus data
      // input nim yang akan dihapus
      printf("Masukan NIM : ");
      scanf("%s", alpro.nim);
      // pemanggilan prosedur update
      deleteData(alpro.nim);
    }

    // jika pilihan bukan 5
    else if (pilihan != 5) {
      printf("Menu yang dipilih tidak ada.\n");
    }

    // pilihan jika sudah selesai proses
    printf("Apakah anda ingin keluar?");
    // input jika ya maka akan keluar program, jika tidak kembali ke tampilan
    // awal
    printf("ya/tidak\n");
    scanf("%s", pilihan2);
    // proses akan terus mengulang jika jawaban bukan ya
  } while (strcmp(pilihan2, "ya") != 0);

  system("cls");  // menghapus layar
  return 0;
}