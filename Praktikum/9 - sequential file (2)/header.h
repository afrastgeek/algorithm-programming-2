/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Latihan Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief Colleger information holder.
 * @details Create anonymous struct which hold student properties.
 * Then define this anonymous struct with "mahasiswa" typedef.
 */
typedef struct {
  char nim[15];   // variabel nim
  char nama[50];  // variabel nama
  char kelas[5];  // variabel kelas
} mahasiswa;      // nama bungkusan

/*--------------------------------------------------------------------------*/
/* Variable declarations                                                    */
/*--------------------------------------------------------------------------*/
mahasiswa data[999];  // variabel bungkusan menampung data
int n;                // banyak data

/*--------------------------------------------------------------------------*/
/* Function and Procedure prototypes                                        */
/*--------------------------------------------------------------------------*/
void readFile();                       // untuk membaca file
void getMaxLength(int max_length[3]);  // untuk mencari record terpanjang
void writeFile();                      // untuk menulis file
int findData(char[15]);                // fungsi untuk mencari data yang sama
void insertData(mahasiswa);            // untuk memasukan data
void deleteData(char[15]);             // untuk menghapus data
void updateData(char[15]);             // untuk mengupdate data
