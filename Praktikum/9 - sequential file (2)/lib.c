/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Latihan Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
| I try to make self commenting code, in which variable name describe itself.
|
*/


#include "header.h"

// proses membaca file
void readFile() {
  n = 0;          // deklarasi n sama dengan nol
  mahasiswa tmp;  // bungkusan penamppung
  FILE *fdata;    // deklarasi nama file
  // membaca file
  fdata = fopen("data/mahasiswa.dat", "r");

  // proses membaca file dan ditampung di bungkusan
  do {
    fscanf(fdata, "%s %s %s", tmp.nim, tmp.nama, tmp.kelas);
    if (strcmp(tmp.nim, "##") != 0 && strcmp(tmp.nama, "##") != 0 &&
        strcmp(tmp.kelas, "##") != 0) {
      data[n] = tmp;
      n += 1;
    }
  } while (strcmp(tmp.nim, "##") != 0 && strcmp(tmp.nama, "##") != 0 &&
           strcmp(tmp.kelas, "##") != 0);
  // menutup file
  fclose(fdata);
}

/**
 * Find the longest record for each attribute (nim, nama, kelas)
 * @return return array of length of longest record.
 */
void getMaxLength(int max_length[3]) {
  int i = 0;
  int temp_length;

  max_length[0] = strlen("NIM");
  max_length[1] = strlen("Nama");
  max_length[2] = strlen("Kelas");

  while (i < n) {
    temp_length = strlen(data[i].nim);
    if (temp_length > max_length[0]) {
      max_length[0] = temp_length;
    }

    temp_length = strlen(data[i].nama);
    if (temp_length > max_length[1]) {
      max_length[1] = temp_length;
    }

    temp_length = strlen(data[i].kelas);
    if (temp_length > max_length[2]) {
      max_length[2] = temp_length;
    }

    i += 1;
  }
}

// proses menulis file
void writeFile() {
  int i;  // variabel iterasi

  FILE *fdata;  // deklarasi fdata
  // menulis di file
  fdata = fopen("data/mahasiswa.dat", "w");
  // proses penulisan file
  for (i = 0; i < n; i += 1) {
    fprintf(fdata, "%s %s %s\n", data[i].nim, data[i].nama, data[i].kelas);
  }
  // proses penambahan record dummy
  fprintf(fdata, "## ## ##\n");
  // menutupi file
  fclose(fdata);
}

// proses mencari data
int findData(char nim[15]) {
  int status = 0;  // deklarasi status sama dengan nol
  int i = 0;       // variabel iterasi

  // memanggil prosedur baca
  readFile();

  // proses mencari
  while (status == 0 && i < n) {
    if (strcmp(data[i].nim, nim) == 0) {
      // jika nilai yang dicari ada
      status = 1;  // status menjadi 1
    } else {
      i += 1;
    }
  }
  // melempar nilai status
  return status;
}

// proses menambah data
void insertData(mahasiswa masukan) {
  int status = findData(masukan.nim);  // menerima lemparan dari proses mencari
  // jika nim tidak ganda
  if (status == 0) {
    // data baru dimasukan
    data[n] = masukan;
    n += 1;
    // data ditulis kedalam file
    writeFile();
    printf("Data berhasil ditambahkan\n");
  } else {
    // jika nim ganda, tidak bisa memasukan data baru
    printf("NIM sudah terdaftar, data gagal ditambahkan\n");
  }
}

// proses menghapus data
void deleteData(char nim[15]) {
  // pemanggilan prosedur baca
  readFile();
  int i, j;        // variabel iterasi
  int status = 0;  // variabel status
  i = 0;
  char hapus[10];  // variabel hapus

  // proses menghapus data
  while (i < n && status == 0) {
    // jika nim yang akan dihapus ada
    if (strcmp(data[i].nim, nim) == 0) {
      status = 1;  // status menjadi satu
      // mencegah jika salah menghapus data
      printf("Data ditemukan. Yakin hapus data?");
      printf("ya/tidak\n");
      // input hapus(ya) atau tidak
      scanf("%s", hapus);
      // jika pilihan ya, maka data dihapus
      if (strcmp(hapus, "ya") == 0) {
        for (j = i; j < n; j += 1) {
          data[j] = data[j + 1];
        }
        n -= 1;
        // data ditulis ke dalam file
        writeFile();
      }
    } else {
      // jika tidak, data tidak akan dihapus
      i += 1;
    }
  }

  // jika nim tidak ada dalam data
  if (status == 0) {
    printf("Data tidak ditemukan\n");
  }
}

// proses update data
void updateData(char nim[15]) {
  // pemanggilan prosedur baca
  readFile();
  int i;        // variabel iterasi
  int status = 0;  // variabel status

  i = 0;
  char nama[25], kelas[15];  // variabel menampung nama dan kelas
  // proses update
  while (i < n && status == 0) {
    // jika nim yang akan diupdate ada di dalam data
    if (strcmp(data[i].nim, nim) == 0) {
      status = 1;
      printf("Data ditemukan\n");
      // tampilan awal sebelum diupdate
      printf("--Data Awal--\n");
      printf("Nama : %s\n", data[i].nama);
      printf("Kelas : %s\n", data[i].kelas);
      printf("-----------\n");
      // input data baru agar data terupdate
      printf("Masukan nama baru : \n");
      scanf("%s", nama);
      printf("Masukan kelas baru : \n");
      scanf("%s", kelas);
      // proses menempatkan ke dalam bungkusan
      strcpy(data[i].nama, nama);
      strcpy(data[i].kelas, kelas);
      printf("Data berhasil diupdate\n");
      // data ditulis kembali ke dalam file
      writeFile();
    } else {
      i += 1;
    }
  }
  // jika nim tidak ada di dalam data
  if (status == 0) {
    printf("Data tidak ditemukan\n");
  }
}