#include "header.h"

void tampil(int n, char string[n][16]) {
	int i;
	for (i = 0; i < n; i += 1) {
		printf("%s ", string[i]);
	}
	printf("\n");
}

void selection(int n, char string[n][16]) {
	int i, j;
	int min_indeks;
	char temp[16];

	// proses pengurutan
	for (i = 0; i < n; i += 1) {
		// inisialisasi indeks minimum
		min_indeks = i;
		// untuk mencari nilai minimum
		for (j = i + 1; j < n; j += 1) {
			if (strcmp(string[j], string[min_indeks]) == -1) {
				min_indeks = j;
			}
		}
		// pertukaran dengan nilai minimum
		strcpy(temp, string[min_indeks]);
		strcpy(string[min_indeks], string[i]);
		strcpy(string[i], temp);

	}
	tampil(n, string);
}