#include "header.h"

int main(int argc, char const *argv[])
{
	int i, n;

	// input
	printf("how much string? [max 16 char]: ");
	scanf("%d", &n);
	char string[n][16];

	printf("insert string : \n");
	for (i = 0; i < n; i += 1) {
		scanf("%s", &string[i]);
	}
	
	selection(n, string); // masuk ke prosedur
	
	return 0;
}