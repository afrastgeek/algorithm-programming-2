#include "header.h"

void tampil(int angka[], int n) {
	int i;
	for (i = 0; i < n; i += 1) {
		printf("%d ", angka[i]);
	}
	printf("\n");
}

void insertion(int angka[], int n) {
	int angka_sisip;
	int i, j;

	// proses pengurutan
	for (i = 0; i < n; i += 1) {
		angka_sisip = angka[i];
		j = i - 1;
		while ((angka_sisip < angka[j]) && (j >= 0)) {
			angka[j + 1] = angka[j];
			j = j - 1;
		}
		angka[j + 1] = angka_sisip;
	}
	tampil(angka,n);
}