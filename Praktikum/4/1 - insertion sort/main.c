#include "header.h"

int main(int argc, char const *argv[])
{
	int i, n;

	// input
	printf("masukkan banyak angka : ");
	scanf("%d", &n);
	int angka[n];

	printf("masukkan angka : \n");
	for (i = 0; i < n; i += 1) {
		scanf("%d", &angka[i]);
	}
	
	insertion(angka, n); // masuk ke prosedur
	
	return 0;
}