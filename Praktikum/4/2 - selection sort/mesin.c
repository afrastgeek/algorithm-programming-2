void tampil(int angka[], int n) {
	int i;
	for (i = 0; i < n; i += 1) {
		printf("%d ", angka[i]);
	}
	printf("\n");
}

void selection(int angka[], int n) {
	int i, j;
	int min_indeks;
	int temp;

	// proses pengurutan
	for (i = 0; i < n; i += 1) {
		// inisialisasi indeks minimum
		min_indeks = i;
		// untuk mencari nilai minimum
		for (j = i + 1; j < n; j += 1) {
			if (angka[j] < angka[min_indeks]) {
				min_indeks = j;
			}
		}
		// pertukaran dengan nilai minimum
		temp = angka[min_indeks];
		angka[min_indeks] = angka[i];
		angka[i] = temp;
	}
	tampil(angka,n);
}