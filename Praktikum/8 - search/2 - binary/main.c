#include "header.h"

int main(int argc, char const *argv[])
{
  int n, i;
  scanf("%d", &n); //masukan jumlah data

  int array[n];
  //proses input data
  for (i = 0; i < n; i++)
  {
    scanf("%d", &array[i]);
  }

  //terlebih dulu urutkan data masukan
  sort(n, array);

  int cariangka;
  scanf("%d", &cariangka); //masukkan nilai yang dicari

  //masuk ke prosedur binary search
  binary(n, array, cariangka);

  return 0;
}