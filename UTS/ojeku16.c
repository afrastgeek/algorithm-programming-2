/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan UTS Algoritma dan Pemrograman II. Jika saya melakukan kecurangan
 *  maka Allah/Tuhan adalah saksi saya, dan saya bersedia menerima
 *hukumannya.
 *  Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define the function (or procedure, whatever).
|
*/
#include "ojeku16.h"

void insertionSort(int n, ojek_data data[]) {
  int i, j;
  ojek_data temp;

  // proses pengurutan
  for (i = 0; i < n; i += 1) {
    temp = data[i];
    j = i - 1;
    while ((temp.komisi_t > data[j].komisi_t) && (j >= 0)) {
      data[j + 1] = data[j];
      j = j - 1;
    }
    data[j + 1] = temp;
  }
}

void selectionSort(int n, ojek_data data[]) {
  int i, j;
  int min_indeks;
  ojek_data temp;

  // proses pengurutan
  for (i = 0; i < n; i += 1) {
    // inisialisasi indeks minimum
    min_indeks = i;
    // untuk mencari nilai minimum
    for (j = i + 1; j < n; j += 1) {
      if (data[j].komisi_t > data[min_indeks].komisi_t) {
        min_indeks = j;
      }
    }
    // pertukaran dengan nilai minimum
    temp = data[min_indeks];
    data[min_indeks] = data[i];
    data[i] = temp;
  }
}

void bubbleSort(int n, ojek_data data[]) {
  /* bubble sort */
  int i, j;
  ojek_data temp;
  int sorted;

  for (i = 1; i < n; i += 1) {  // repeat bubble pass n-1 times
    sorted = 1;  // break when no changes in a pass => already sorted

    j = 0;
    while (j < n - i && sorted) {
      if (data[j].komisi_t < data[j + 1].komisi_t) {  // n-i comparisons
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
        sorted = 0;
      }
      j += 1;
    }
  }  // so no need for further bubble passes
}

void quick(int awal, int tengah, ojek_data data[]) {
  int i, p;

  ojek_data temp;
  ojek_data temp2;
  ojek_data poros;

  i = awal;
  p = tengah;
  poros = data[(awal + tengah) / 2];

  do {
    while (data[i].komisi_t < poros.komisi_t &&
           i <= p) {  // pengecekkan jika data awal lebih besar dari tengah
      i++;
    }
    while (data[p].komisi_t > poros.komisi_t && i <= p) {
      p--;
    }

    if (i < p) {  // ketika i < p menunjukkan bahwa nilai data ke i, lebih kecil
                  // dari data ke p. Maka dilakukanlah pergantian
      temp = data[i];
      temp2 = data[i];

      data[i] = data[p];
      data[i] = data[p];

      data[p] = temp;
      data[p] = temp2;

      i += 1;
      p -= 1;
    }
  } while (i < p);

  if ((p > awal) && (p < tengah)) {  // ketika p masih belum berada di kanan,
                                     // ulangi kembali langkah
    quick(awal, p, data);
  }

  if ((i > awal) && (i < tengah)) {  // sama
    quick(i, tengah, data);
  }
}

void sortThis(char sort_by[], int n, ojek_data to_sort[n]) {
  if (strcmp(sort_by, "insert") == 0) {
    insertionSort(n, to_sort);
  } else if (strcmp(sort_by, "select") == 0) {
    selectionSort(n, to_sort);
  } else if (strcmp(sort_by, "bubble") == 0) {
    bubbleSort(n, to_sort);
  } else if (strcmp(sort_by, "quick") == 0) {
    insertionSort(n, to_sort);
  }
}

void mergeThis(ojek_data result[], ojek_data data1[], ojek_data data2[], int n,
               int m) {
  int i, j, k, l;
  i = j = k = 0;

  while (i < n && j < m) {
    if (data1[i].komisi_t > data2[j].komisi_t) {
      result[k] = data1[i];
      i += 1;
      k += 1;
    } else if (data2[j].komisi_t > data1[i].komisi_t) {
      result[k] = data2[j];
      j += 1;
      k += 1;
    } else {
      result[k] = data1[i];
      i += 1;
      k += 1;
      result[k] = data2[j];
      j += 1;
      k += 1;
    }
  }
  /* clearing */
  if (i < n) {
    for (l = i; l < n; l += 1) {
      result[k] = data1[l];
      k += 1;
    }
  }
  if (j < m) {
    for (l = j; l < m; l += 1) {
      result[k] = data2[l];
      k += 1;
    }
  }
}