/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan UTS Algoritma dan Pemrograman II. Jika saya melakukan kecurangan
 *  maka Allah/Tuhan adalah saksi saya, dan saya bersedia menerima
 *hukumannya.
 *  Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register needed libraries, and function.
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief The pattern block properties.
 * @details This struct hold an array of 5 integer which will be filled by
 * lastRowSpace function.
 *
 */
typedef struct ojek_data {/* Struct declaration */
  char nama_driver_t[32];
  char nomor_telepon_t[32];
  int jam_terbang_t;
  double komisi_t;
} ojek_data; /* Altering Struct with Typedef declaration */

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
void insertionSort(int n, ojek_data data[]);
void selectionSort(int n, ojek_data data[]);
void bubbleSort(int n, ojek_data data[]);
void quick(int awal, int tengah, ojek_data data[]);
void sortThis(char sort_by[], int n, ojek_data to_sort[n]);
void mergeThis(ojek_data result[], ojek_data data1[], ojek_data data2[], int n,
               int m);