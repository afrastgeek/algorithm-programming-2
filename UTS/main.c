/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan UTS Algoritma dan Pemrograman II. Jika saya melakukan kecurangan
 *  maka Allah/Tuhan adalah saksi saya, dan saya bersedia menerima
 *hukumannya.
 *  Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "ojeku16.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  char sort_by[32];
  int num_of_data[3];
  int index;

  scanf(" %s", sort_by); /* method to sort by komisi_t */

  scanf("%d", &num_of_data[0]);
  ojek_data fou_teen[num_of_data[0]];

  for (index = 0; index < num_of_data[0]; index += 1) {
    scanf(" %s %s %d %lf", fou_teen[index].nama_driver_t,
          fou_teen[index].nomor_telepon_t, &fou_teen[index].jam_terbang_t,
          &fou_teen[index].komisi_t);
  }

  scanf("%d", &num_of_data[1]);
  ojek_data fiv_teen[num_of_data[1]];

  for (index = 0; index < num_of_data[1]; index += 1) {
    scanf(" %s %s %d %lf", fiv_teen[index].nama_driver_t,
          fiv_teen[index].nomor_telepon_t, &fiv_teen[index].jam_terbang_t,
          &fiv_teen[index].komisi_t);
  }

  scanf("%d", &num_of_data[2]);
  ojek_data six_teen[num_of_data[2]];

  for (index = 0; index < num_of_data[2]; index += 1) {
    scanf(" %s %s %d %lf", six_teen[index].nama_driver_t,
          six_teen[index].nomor_telepon_t, &six_teen[index].jam_terbang_t,
          &six_teen[index].komisi_t);
  }

  sortThis(sort_by, num_of_data[0], fou_teen);
  sortThis(sort_by, num_of_data[1], fiv_teen);
  sortThis(sort_by, num_of_data[2], six_teen);

  /* Merge! */
  ojek_data temp[num_of_data[0] + num_of_data[1]];
  mergeThis(temp, fou_teen, fiv_teen, num_of_data[0], num_of_data[1]);

  int final_length = (num_of_data[0] + num_of_data[1]) + num_of_data[2];
  ojek_data final[final_length];
  mergeThis(final, temp, six_teen, (num_of_data[0] + num_of_data[1]),
            num_of_data[2]);

  for (index = 0; index < final_length; index += 1) {
    printf("%s %s %d %.0f\n", final[index].nama_driver_t,
           final[index].nomor_telepon_t, final[index].jam_terbang_t,
           final[index].komisi_t);
  }

  return 0; /* return 0 as the program succeed */
} /* main function */
