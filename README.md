# Algorithm & Programming 2 Course Source Code

This is a bunch of source code i write for Algorithm & Programming 2 Course at
UPI.

The Course using C Language.

## File Structure

Browse the file at [https://gitlab.com/afrastgeek/algorithm-programming-2/tree/master](https://gitlab.com/afrastgeek/algorithm-programming-2/tree/master)

- Evaluasi Praktikum, Practice Class test.
- Meet, what i write on class.
- Praktikum, what i write on practice class.
- Quiz
- Responsi, what i write on responsi class.
- [Tugas Masa Depan](https://gitlab.com/afrastgeek/algorithm-programming-2/tree/master/Tugas%20Masa%20Depan), final assignment task.
- Tugas Praktikum, Practice Class Assignment.
- UAS, Post-semester test.
- UTS, Mid-semester test.