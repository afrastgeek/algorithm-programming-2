#include <stdio.h>

typedef struct {
    int x;
    int y;
} titik;

int main(int argc, char const* argv[])
{
    titik matriks[4][4];
    int i, j;
    int n;

    for (i = 0; i < 4; i += 1) {
        for (j = 0; j < 4; j += 1) {
            printf("\nmasukkan koordinat x: \n");
            scanf("%d", &matriks[i][j].x);
            printf("\nmasukkan koordinat y: \n");
            scanf("%d", &matriks[i][j].y);
        }
    }

    for (i = 0; i < 4; i += 1) {
        for (j = 0; j < 4; j += 1) {
            printf("\nkoordinat x: %d\n", matriks[i][j].x);
            printf("\nkoordinat y: %d\n", matriks[i][j].y);
        }
    }

    return 0;
}