#include <stdio.h>

void boardLayout(int board_size, int board[board_size][board_size])
{
    /*
        Variable Declaration
    */
    int column, row;

    /*
        Displaying Board
    */
    /* Board Text */
    printf("Board Status:\n");

    /*  Column Legend  */
    printf("    ");
    for (column = 0; column < board_size; column += 1) {
        printf("%d   ", column + 1); // column legend humanization
    }
    printf("\n");

    /*  Top Row Border  */
    printf("  ");
    for (column = 0; column < board_size; column += 1) {
        printf("+---");
    }
    printf("+\n");

    /*  Column  */
    for (row = 0; row < board_size; row += 1) {
        /*  Row Legend and Left Border  */
        printf("%d | ", row + 1);   // row legend humanization
        for (column = 0; column < board_size; column += 1) {
            if (board[row][column] == 0) { // when the square is empty(0)
                printf("  | ");
            }
            else if (board[row][column] == 1) { // filled by 1st player
                printf("O | ");
            }
            else if (board[row][column] == 2) { // filled by 2nd player
                printf("X | ");
            }
        }

        /*  Bottom Row Border   */
        printf("\n  ");
        for (column = 0; column < board_size; column += 1) {
            printf("+---");
        }
        printf("+\n");
    }
    printf("\n");
}

int main(int argc, char const* argv[])
{
    /*
        Variable Declaration
    */
    int board_size = 3; // default board_size is 3
    int row, column;
    int player;
    int x, y; // player will play the game by mentioning square coordinate
    int turn;
    int game_status; // game will end when status 0
    int input_status; // player input will accepted when status 0

    /*
        Load Start Screen
    */
    printf("TIC TAC TOE v1\n- afr -\n\n\n3 line tic tac toe!\n\n");
    printf("Initializing Tic Tac Toe Board...\n\n");
    printf("Insert Board Size! [3 * 3]\n"); // notification for input
    scanf("%d", &board_size); // prompt user to input desired board size
    system("cls"); // clear the screen

    /*
        Board Declaration
    */
    int board[board_size][board_size];

    //  Initialize Board Value
    for (row = 0; row < board_size; row += 1) {
        for (column = 0; column < board_size; column += 1) {
            board[row][column] = 0; // fill each square with a zero value
        }
    }

    /*
        Player & Coordinates Initialization

        At the first, the player and coordinates variable set as "-1" to mark
        that the game still not started yet.
    */
    player = x = y = -1;

    /*
        Gameplay
    */
    game_status = 1; // The game started, will not end before status reach "0"
    while (game_status) {
        /*  Display the Board   */
        boardLayout(board_size, board);

        /*  Player switching    */
        if (player == 1) {
            player = 2;
        }
        else {
            player = 1;
        }

        /*  Ask player to input the coordinates     */
        input_status = 1; // player input are marked as invalid
        do {
            printf("\nPlayer %d,\nInsert your coordinates! (x y): ", player);
            scanf("%d %d", &x, &y);
            /* Coordinates humanization */
            x -= 1; 
            y -= 1;

            /*  Player Input Validator  */
            if (board[y][x] == 0) { // when player selecting empty square...
                board[y][x] = player; // fill the square based on player input
                input_status = 0; // player input are valid
            }
        } while (input_status);

        /* Match End Checker */
        if ((player == board[y][x - 1] && player == board[y][x - 2]) // match at the end of column
            || (player == board[y - 1][x] && player == board[y - 2][x]) // match at the end of row
            || (player == board[y - 1][x - 1] && player == board[y - 2][x - 2]) // match at the end of -1 diagonal
            || (player == board[y][x + 1] && player == board[y][x + 2]) // match at the first of column
            || (player == board[y + 1][x] && player == board[y + 2][x]) // match at the first of row
            || (player == board[y + 1][x + 1] && player == board[y + 2][x + 2]) // match at the first of -1 diagonal
            || (player == board[y - 1][x + 1] && player == board[y - 2][x + 2]) // match at the end of +1 diagonal
            || (player == board[y + 1][x - 1] && player == board[y + 2][x - 2]) // match at the end of +1 diagonal
            || (player == board[y - 1][x] && player == board[y + 1][x] && player == board[y][x - 1] && player == board[y + 1][x]) // match at the center
            ) {
            game_status = 0; // game's over
        }

        system("cls"); // clear the screen
    }

    /*
        Load End Screen
    */
    boardLayout(board_size, board);
    printf("CONGRATULATION!\nPlayer %d WIN!\n\nGAME'S OVER\n", player);

    return 0;
}