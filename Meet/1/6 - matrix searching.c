#include <stdio.h>

int main(int argc, char const* argv[])
{
    int matriks[4][4];

    int baris;
    int kolom;

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            printf("masukkan angka:\n");
            scanf("%d", &matriks[baris][kolom]);
            printf("\n");
        }
    }

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            printf("%d ", matriks[baris][kolom]);
        }
        printf("\n");
    }

    int what_to_find;
    printf("Masukkan angka yang akan dicari\n");
    scanf("%d", &what_to_find);

    int is_found;
    //	add searching mechanism
    baris = is_found = 0;
    while (baris < 4 && is_found == 0) {
        kolom = 0;
        while (kolom < 4 && is_found == 0) {
            if (matriks[baris][kolom] == what_to_find) {
                is_found = 1;
                printf("Ada!\n");
            }
            kolom += 1;
        }
        baris += 1;
    }

    return 0;
}