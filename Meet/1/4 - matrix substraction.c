#include <stdio.h>

int main(int argc, char const* argv[])
{
    int preferred_row, preferred_column;

    printf("insert preferred row number for each matrix:\n");
    scanf("%d", &preferred_row);
    printf("insert preferred column number for each matrix:\n");
    scanf("%d", &preferred_column);

    int matriks1[preferred_row][preferred_column];
    int matriks2[preferred_row][preferred_column];
    int matriks3[preferred_row][preferred_column];

    int row;
    int column;

    for (row = 0; row < preferred_row; row += 1) {
        for (column = 0; column < preferred_column; column += 1) {
            printf("value for matrix1 (%d,%d) : ", column + 1, row + 1);
            scanf("%d", &matriks1[row][column]);
            printf("\n");
        }
    }
    for (row = 0; row < preferred_row; row += 1) {
        for (column = 0; column < preferred_column; column += 1) {
            printf("value for matrix2 (%d,%d) : ", column + 1, row + 1);
            scanf("%d", &matriks2[row][column]);
            printf("\n");
        }
    }

    printf("result matrix\n");
    for (row = 0; row < preferred_row; row += 1) {
        printf("| ");
        for (column = 0; column < preferred_column; column += 1) {
            matriks3[row][column] = matriks1[row][column] - matriks2[row][column];
            printf("%d ", matriks3[row][column]);
        }
        printf("|\n");
    }

    return 0;
}