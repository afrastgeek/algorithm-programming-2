#include <stdio.h>

int main(int argc, char const* argv[])
{
    int matriks1[4][4];
    int matriks2[4][4];

    int baris;
    int kolom;

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            printf("masukkan angka:\n");
            scanf("%d", &matriks1[baris][kolom]);
            printf("\n");
        }
    }

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            matriks2[baris][kolom] = matriks1[baris][kolom];
        }
    }

    return 0;
}