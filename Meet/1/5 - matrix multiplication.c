#include <stdio.h>

int main(int argc, char const* argv[])
{
    int preferred_row[2], preferred_column[2];
    int row, column;
    int selector;

    printf("insert preferred row number for matrix1:\n");
    scanf("%d", &preferred_row[0]);
    printf("insert preferred column number for matrix1 and row number for matrix2:\n");
    scanf("%d", &preferred_column[0]);
    preferred_row[1] = preferred_column[0];
    printf("insert preferred column number for matrix2:\n");
    scanf("%d", &preferred_column[1]);

    int matriks1[preferred_row[0]][preferred_column[0]];
    int matriks2[preferred_row[1]][preferred_column[1]];
    int matriks3[preferred_row[0]][preferred_column[1]];

    for (row = 0; row < preferred_row[0]; row += 1) {
        for (column = 0; column < preferred_column[0]; column += 1) {
            printf("value for matrix1 (%d,%d) : ", column + 1, row + 1);
            scanf("%d", &matriks1[row][column]);
            printf("\n");
        }
    }
    for (row = 0; row < preferred_row[1]; row += 1) {
        for (column = 0; column < preferred_column[1]; column += 1) {
            printf("value for matrix2 (%d,%d) : ", column + 1, row + 1);
            scanf("%d", &matriks2[row][column]);
            printf("\n");
        }
    }

    printf("matrix1 layout\n");
    for (row = 0; row < preferred_row[0]; row += 1) {
        printf("| ");
        for (column = 0; column < preferred_column[0]; column += 1) {
            printf("%d ", matriks1[row][column]);
        }
        printf("|\n");
    }
    printf("\n");
    printf("matrix2 layout\n");
    for (row = 0; row < preferred_row[1]; row += 1) {
        printf("| ");
        for (column = 0; column < preferred_column[1]; column += 1) {
            printf("%d ", matriks2[row][column]);
        }
        printf("|\n");
    }
    printf("\n");

    printf("result matrix\n");
    for (row = 0; row < preferred_row[0]; row += 1) {
        printf("| ");
        for (column = 0; column < preferred_column[1]; column += 1) {
            matriks3[row][column] = 0;
            for (selector = 0; selector < preferred_column[0]; selector += 1) {
                matriks3[row][column] = matriks3[row][column] + matriks1[row][selector] * matriks2[selector][column];
            }
            printf("%d ", matriks3[row][column]);
        }
        printf("|\n");
    }

    return 0;
}