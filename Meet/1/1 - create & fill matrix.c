#include <stdio.h>

int main(int argc, char const* argv[])
{
    int matriks[4][4];

    int baris;
    int kolom;

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            printf("masukkan angka:\n");
            scanf("%d", &matriks[baris][kolom]);
            printf("\n");
        }
    }

    for (baris = 0; baris < 4; baris += 1) {
        for (kolom = 0; kolom < 4; kolom += 1) {
            printf("%d ", matriks[baris][kolom]);
        }
        printf("\n");
    }

    return 0;
}