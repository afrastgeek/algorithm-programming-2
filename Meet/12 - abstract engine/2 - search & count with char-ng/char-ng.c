#include "char-ng.h"

void START(char pita[]) {
  indeks = 0;
  cc = pita[indeks];
}

void INC(char pita[]) {
  indeks += 1;
  cc = pita[indeks];
}

void ADV(char pita[]) {
  indeks += 1;
  cc = pita[indeks];
  while(cc == ' ' && EOP() == 0) {
    indeks += 1;
    cc = pita[indeks];
  }
}

char GETCC() {
  return cc;
}

int EOP() {
  if(cc == '.') {
    return 1;
  } else {
    return 0;
  }
}