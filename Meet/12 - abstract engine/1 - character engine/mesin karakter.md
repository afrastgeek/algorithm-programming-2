## Mesin Karakter

- CC : Current Character
- START : Menyalakan mesin, dan masuk satu karakter
- INC : Increment, Maju satu karakter
- ADV : Advance, Maju sampai ditemukan karakter berikutnya (spasi dilewat)
- GET CC : Mengambil nilai CC
- EOP : End of Process
