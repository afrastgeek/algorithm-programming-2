#include "char-ng.h"

int main(int argc, char const *argv[])
{
  char pita[50];
  printf("Masukkan pada kalimat pada pita\n");
  scanf("%49[^\n]s", &pita);

  START(pita);
  printf("%c\n", GETCC());

  while(EOP() == 0) {
    INC(pita);
    printf("%c\n", GETCC());
  }

  return 0;
}