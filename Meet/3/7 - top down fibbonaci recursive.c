#include <stdio.h>

int fib2(int* arr, int n)
{ // top down
    if (n < 2) {
        arr[n] = n;
        return n;
    }
    else {
        arr[n] = fib2(arr, n - 1) + fib2(arr, n - 2);
        return arr[n];
    }
}

int main(int argc, char const* argv[])
{
    int n;

    printf("masukkan bilangan fibonaci ");
    scanf("%d", &n);
    int arr[n + 1];

    fib2(arr, n);
    int i;
    for (i = 0; i < n + 1; i += 1) {
        printf("%d ", arr[i]);
    }

    return 0;
}