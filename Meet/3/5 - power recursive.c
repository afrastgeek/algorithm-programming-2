#include <stdio.h>

/*
actually, this function have the same name with the built in
c compiler function, pow.
*/
int pow(int x, int y)
{
    int hasil;

    if (y == 0) {
        return 1;
    }
    else {
        hasil = x * pow(x, y - 1);
    }
    return hasil;
}

int main()
{
    int first_value, second_value;
    scanf("%d", &first_value);
    scanf("%d", &second_value);

    int hasil = pow(first_value, second_value);
    printf("hasil pangkat: %d\n", hasil);

    return 0;
}