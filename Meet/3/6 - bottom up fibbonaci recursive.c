#include <stdio.h>

void fib(int* arr, int n, int i)
{ // bottom up
    if (i == 0) {
        arr[i] = 0;
        fib(arr, n, i + 1);
    }
    else if (i == 1) {
        arr[i] = 1;
        fib(arr, n, i + 1);
    }
    else {
        if (i < n) {
            arr[i] = arr[i - 1] + arr[i - 2];
            fib(arr, n, i + 1);
        }
    }
}

int main(int argc, char const* argv[])
{
    int n;

    printf("masukkan bilangan fibonaci ");
    scanf("%d", &n);
    int arr[n + 1];

    fib(arr, n, 0);
    int i;
    for (i = 0; i < n + 1; i += 1) {
        printf("%d ", arr[i]);
    }

    return 0;
}