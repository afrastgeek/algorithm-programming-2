#include <stdio.h>

int faktorial(int n)
{
    printf("n = %d\n", n);
    if ((n == 0) || (n == 1)) {
        return 1;
    }
    else {
        printf("else %d\n", n * faktorial(n - 1));
        return (n * faktorial(n - 1));
    }
}

int main(int argc, char const* argv[])
{
    int value;
    scanf("%d", &value);
    int hasil = faktorial(value);
    printf("hasil faktorial: %d\n", hasil);

    return 0;
}