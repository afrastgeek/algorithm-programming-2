#include <stdio.h>

int sum(int n)
{
    printf("n = %d\n", n);

    if (n == 1) {
        return 1;
    }
    else {
        return (n + sum(n - 1));
    }
}

int main(int argc, char const* argv[])
{
    int value;
    scanf("%d", &value);
    int hasil = sum(value);
    printf("hasil sum: %d\n", hasil);

    return 0;
}