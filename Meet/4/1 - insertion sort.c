#include <stdio.h>

int main() {
    int tabInt[10] = {34, 67, 23, 28, 98, 15, 89, 67, 28, 18};
    int i, data_sisip, j;
    for(i=1; i<10; i += 1){
        data_sisip = tabInt[i];
        j = i - 1;
        while((data_sisip < tabInt[j]) && (j >= 0)) {
        // for descending
        // while((data_sisip > tabInt[j]) && (j >= 0)) {
            /* jika data array lebih kecil dari data sisip maka data array digeser ke belakang */
            tabInt[j + 1] = tabInt[j];
            j -= 1;
        }
        /* menempatkan data sisip pada array */
        tabInt[j + 1] = data_sisip;
    }
    return 0;
}