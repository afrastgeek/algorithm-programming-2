/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Evaluasi Praktikum 2, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
typedef struct row {
  char left_t[32];
  char right_t[32];
} row;

/*--------------------------------------------------------------------------*/
/* Variable declarations                                                    */
/*--------------------------------------------------------------------------*/
char file[] = "data/tkursi.txt";

/*--------------------------------------------------------------------------*/
/* Macro declarations                                                       */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* Procedure prototypes                                                     */
/*--------------------------------------------------------------------------*/
