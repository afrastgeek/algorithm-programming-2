/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Evaluasi Praktikum 2, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "header.h"

// proses membaca file
void readFile(row data[], int num_of_row) {
  num_of_row = 0;          // deklarasi num_of_row sama dengan nol
  row tmp;
  FILE *fdata;    // deklarasi nama file
  // membaca file
  fdata = fopen("data/tkursi.txt", "r");

  // proses membaca file dan ditampung di bungkusan
  do {
    fscanf(fdata, "%s %s", tmp.left_t, tmp.right_t);
    if (!strcmp(tmp.left_t, "dummy") && !strcmp(tmp.right_t, "dummy")) {
      data[num_of_row] = tmp;
      num_of_row += 1;
    }
  } while (!strcmp(tmp.left_t, "dummy") && !strcmp(tmp.right_t, "dummy"));
  // menutup file
  fclose(fdata);
}

// proses menulis file
void writeFile(row data[], int num_of_row) {
  int i;  // variabel iterasi

  FILE *fdata;  // deklarasi fdata
  // menulis di file
  fdata = fopen("data/tkursi.txt", "w");
  // proses penulisan file
  for (i = 0; i < num_of_row; i += 1) {
    fprintf(fdata, "%s %s\n", data[i].left_t, data[i].right_t);
  }
  // proses penambahan record dummy
  fprintf(fdata, "dummy dummy\n");
  // menutupi file
  fclose(fdata);
}