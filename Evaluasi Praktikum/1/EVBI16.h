/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Evaluasi Praktikum 1, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/

#include <stdio.h>
#include <string.h>

/*
    DEFINE THRESHOLD NUMBER
*/
#ifndef THRESHOLD
#define THRESHOLD 100
#endif

/*
    TYPE DEFINITION
*/
typedef struct {
  char bin[16];
  int dec;
} numeric;

/*
    FUNCTION AND PROCEDURE DECLARATION
*/
int convBinary(char bin[]);
void insertionSort(numeric angka[], int n);
void mergeThis(numeric result[], numeric data1[], numeric data2[], int n,
               int m);
int searchThis(numeric data[], int n, int to_search);