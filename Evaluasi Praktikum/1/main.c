/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Evaluasi Praktikum 1, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "EVBI16.h"

int main(int argc, char const *argv[]) {
  /*
    DECLARE VARIABLE
  */
  numeric lower[32], higher[32], result[32];
  char temp[16];
  int i, lo, hi;
  int decimal;
  int find_this;
  int num_of_result;

  lo = hi = 0;

  /*
    INPUT SECTION
  */
  do {
    scanf(" %s", temp);
    if (strcmp(temp, "Selesai")) {
      decimal = convBinary(temp);
      if (decimal <= THRESHOLD) {
        strcpy(lower[lo].bin, temp);
        lower[lo].dec = decimal;
        lo += 1;
      } else if (decimal > THRESHOLD) {
        strcpy(higher[hi].bin, temp);
        higher[hi].dec = decimal;
        hi += 1;
      }
    }
  } while (strcmp(temp, "Selesai"));
  scanf("%d", &find_this);

  num_of_result = hi + lo;

  /*
    SORT EACH ARRAY
  */
  insertionSort(lower, lo);
  insertionSort(higher, hi);

  /*
    MERGE THE ARRAY
  */
  mergeThis(result, lower, higher, lo, hi);

  /*
    SEARCH NUMBER INDEX
  */
  int hasil = searchThis(result, num_of_result, find_this);

  /*
    PRINT
  */
  if (hasil != -1) {
    for (i = 0; i < num_of_result; i += 1) {
      printf("%s\n", result[i].bin);
    }
    printf("index %d\n", hasil + 1);
  } else {
    printf("data not found\n");
  }

  return 0;
}
