/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Evaluasi Praktikum 1, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "EVBI16.h"

int convBinary(char bin[]) {
  int i;
  int power;
  int length = strlen(bin);
  int result;

  result = 0;
  power = 1;

  for (i = length - 1; i >= 0; i -= 1) {
    if (bin[i] == '1') {
      result += power;
    }
    power *= 2;
  }

  return result;
}

void insertionSort(numeric angka[], int n) {
  numeric temp;
  int i, j;

  // proses pengurutan
  for (i = 0; i < n; i += 1) {
    temp = angka[i];
    j = i - 1;
    while ((temp.dec < angka[j].dec) && (j >= 0)) {
      angka[j + 1] = angka[j];
      j -= 1;
    }
    angka[j + 1] = temp;
  }
}

void mergeThis(numeric result[], numeric data1[], numeric data2[], int n,
               int m) {
  int i, j, k, l;
  i = j = k = 0;

  while (i < n && j < m) {
    if (data1[i].dec < data2[j].dec) {
      result[k] = data1[i];
      i += 1;
      k += 1;
    } else if (data2[j].dec < data1[i].dec) {
      result[k] = data2[j];
      j += 1;
      k += 1;
    } else {
      result[k] = data1[i];
      i += 1;
      k += 1;
      result[k] = data2[j];
      j += 1;
      k += 1;
    }
  }
  /* clearing */
  if (i < n) {
    for (l = i; l < n; l += 1) {
      result[k] = data1[l];
      k += 1;
    }
  }
  if (j < m) {
    for (l = j; l < m; l += 1) {
      result[k] = data2[l];
      k += 1;
    }
  }
}

int searchThis(numeric data[], int n, int to_search) {
  int i = 0;
  int found = 1;
  while (found && i < n) {
    if (data[i].dec == to_search) {
      found = 0;
      return i;
    }
    i += 1;
  }
  return -1;
}