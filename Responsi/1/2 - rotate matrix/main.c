#include "header.h"

int main(int argc, char const* argv[])
{
    int num_of_row, num_of_column;

    scanf("%d", &num_of_row);
    scanf("%d", &num_of_column);

    int matrix[num_of_row][num_of_column];
    int temp[100][100];

    int i, j, k, n;

    for (i = 0; i < num_of_row; i += 1) {
        for (j = 0; j < num_of_column; j += 1) {
            scanf("%d", &matrix[i][j]);
        }
    }

    printf("\nBerapa kali putar?\n");
    scanf("%d", &n);

    for (i = 0; i < n; i += 1) {
        for (j = 0; j < num_of_row; j += 1) {
            for (k = 0; k < num_of_column; k += 1) {
                temp[j][k] = matrix[(num_of_row - 1) - k][j];
            }
        }
        for (j = 0; j < num_of_row; j += 1) {
            for (k = 0; k < num_of_column; k += 1) {
                matrix[j][k] = temp[j][k];
            }
        }
    }

    for (i = 0; i < num_of_row; i += 1) {
        for (j = 0; j < num_of_column; j += 1) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}