#include "header.h"

int main(int argc, char const *argv[])
{
    int num_of_row, num_of_column;

    scanf("%d", &num_of_row);
    scanf("%d", &num_of_column);

    int matrix[num_of_row][num_of_column];

    int i, j;

    for (i = 0; i < num_of_row; i += 1) {
        for (j = 0; j < num_of_column; j += 1) {
            scanf("%d", &matrix[i][j]);
        }
    }

    for (i = 0; i < num_of_row; i += 1) {
        for (j = 0; j < num_of_column; j += 1) {
            printf("%d ", evenAddition(matrix[i][j]));
        }
        printf("\n");
    }

    return 0;
}