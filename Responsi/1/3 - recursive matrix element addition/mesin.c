#include "header.h"

int evenAddition(int x)
{
    int jumlah;

    if (x % 2 == 0) {
        jumlah = x;
    }
    else {
        jumlah = x - 1;
    }

    if (x >= 0) {
        jumlah += evenAddition(x - 2);
    }
    else {
        return 0;
    }

    return jumlah;
}