#include "header.h"

int NTE(int a, int b, int c)
{
    if ((a > b && a < c) || (a > c && a < b)) {
        return a;
    }
    else if ((b > a && b < c) || (b > c && a < b)) {
        return b;
    }
    else if ((c > a && b < c) || (b > c && c < a)) {
        return c;
    } else {
        return -1;
    }
}