#include "header.h"

int main(int argc, char const* argv[])
{
    int num_of_row, num_of_column;

    scanf("%d", &num_of_row);
    scanf("%d", &num_of_column);

    bungkusan matriks[num_of_row][num_of_column];

    int row, column;
    for (row = 0; row < num_of_row; row += 1) {
        for (column = 0; column < num_of_column; column += 1) {
            scanf("%d", &matriks[row][column].angka1);
            scanf("%d", &matriks[row][column].angka2);
            scanf("%d", &matriks[row][column].angka3);
        }
    }

    for (row = 0; row < num_of_row; row += 1) {
        for (column = 0; column < num_of_column; column += 1) {
            printf("%d", NTE(matriks[row][column].angka1,matriks[row][column].angka2,matriks[row][column].angka3));
        }
    }

    return 0;
}