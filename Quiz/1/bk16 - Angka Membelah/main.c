/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 1 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "header.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const *argv[]) {
  /*
    Variable Declaration
  */
  int width, length; /* how much the matrix size is? */
  int row, column;   /* matrix element selector */

  scanf("%d %d", &width, &length); /* get the size of the matrix! */

  int matrix_of_num[width][length]; /* define matrix with specified size */

  /* this matrix use "amoeboid" typedef */
  amoeboid number[width][length]; /* define another matrix with same size */

  /*
    Input Section
  */
  for (row = 0; row < width; row += 1) { /* foreach matrix row ... */
    for (column = 0; column < length; column += 1) { /* and matrix column */
      scanf("%d", &matrix_of_num[row][column]);      /* get the number */
      number[row][column].first_num_t = matrix_of_num[row][column]; /* copy! */
      /* how much it could be divided? */
      number[row][column].max_split_t = splitNumber(matrix_of_num[row][column]);
    } /* for length */
  }   /* for width */

  /*
    Print the Result!
  */
  for (row = 0; row < width; row += 1) { /* foreach matrix row ... */
    for (column = 0; column < length; column += 1) { /* and matrix column */
      /* print the result */
      printf("(%d, %d)", number[row][column].first_num_t,
             number[row][column].max_split_t); /* the number and it division */
      if (column != length - 1) {
        printf(" "); /* only print space when it's not the last element */
      }              /* if column not the last */
    }                /* for length */
    printf("\n");    /* never forget to print enter character */
  }                  /* for width */

  return 0; /* return 0 as the program succeed*/
} /* main function */
