/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 1 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "header.h"

/**
 * @brief Find out how much the number can be splitted with threshold 1
 * @details This function will divide the num parameter and calculate how much
 * it could be divided until the value are lower than 1.
 *
 * @param num The number to be split.
 * @return How much the number could be split until the value lower than 1
 */
int splitNumber(int num) {
  if (num >= 1) {                /* if the number are 1 or greater ... */
    num /= 2;                    /* divide by two */
    return 1 + splitNumber(num); /* then divide it again, and again ... */
    /* return how much it could be divided */
  } else {    /* otherwise */
    return 0; /* return 0, and stop dividing! */
  }           /* if num is same or greater than 1 */
} /* splitNumber */
