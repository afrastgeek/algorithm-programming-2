/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 1 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/

#include <stdio.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief The amoeboid number holder.
 * @details Create anonymous struct which hold a number and how much it could
 * be divided (will use "splitNumber" function later). Then define this
 * anonymous struct with "amoeboid" typedef.
 */
typedef struct {
  int first_num_t; /* the number */
  int max_split_t; /* the calculated division */
} amoeboid;

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
int splitNumber(int num);
