/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 2 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "header.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, and pattern display are executed.
 *
 * @param argc The parameter are additional. void also could be used.
 * @param argv The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an int
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  /* VARIABLE DECLARATION */
  int num_of_menu;   /* number of menu to operate with */
  int item;          /* item iterator */
  char to_search[8]; /* food properties kind to search for */

  scanf("%d", &num_of_menu); /* get num_of_menu */

  /* ANOTHER VARIABLE DECLARATION */
  food_properties menu[num_of_menu]; /* initialize menu */

  /* INPUT SECTION */
  for (item = 0; item < num_of_menu; item += 1) {
    scanf("%s %s %d", menu[item].food_code_t, menu[item].food_name_t,
          &menu[item].food_price_t); /* get food menu properties */
  }

  scanf("%s", to_search); /* get fod properties kind to search for */

  if (strcmp(to_search, "kode") == 0) {
    char search_for[16];
    scanf("%s", search_for); /* get string data to search for */
    insertionSort(menu, num_of_menu, to_search);
    binarySearchString(num_of_menu, menu, to_search, search_for);
  } else if (strcmp(to_search, "nama") == 0) {
    char search_for[64];
    scanf("%s", search_for); /* get string data to search for */
    insertionSort(menu, num_of_menu, to_search);
    binarySearchString(num_of_menu, menu, to_search, search_for);
  } else if (strcmp(to_search, "harga") == 0) {
    int search_for;
    scanf("%d", &search_for); /* get integer data to search for */
    insertionSort(menu, num_of_menu, to_search);
    binarySearchInt(num_of_menu, menu, to_search, search_for);
  }

  return 0;
}
