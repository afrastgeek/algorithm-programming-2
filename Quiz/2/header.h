/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 2 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, global
| struct, and function and procedure.
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
typedef struct food_properties {
  char food_code_t[16];
  char food_name_t[64];
  int food_price_t;
} food_properties;

/*--------------------------------------------------------------------------*/
/* Procedure prototypes                                                     */
/*--------------------------------------------------------------------------*/
void insertionSort(food_properties data[], int num_of_item, char to_sort[]);

/* I don't know how to search from different datatype with only one function */
void binarySearchString(int num_of_data,
                        food_properties data[],
                        char to_search[],
                        char search_for[]);
void binarySearchInt(int num_of_data,
                     food_properties data[],
                     char to_search[],
                     int search_for);