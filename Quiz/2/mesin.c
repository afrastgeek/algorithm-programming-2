/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan Kuis 2 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure.
|
*/

#include "header.h"

/**
 * insertionSort, like it's name, will sort item with insertion sort method.
 * @param data        array of data to sort.
 * @param num_of_data how much the data is?
 * @param to_sort     what to sort?
 *
 * TODO: don't pass "to_sort" variables. should know what data's datatype.
 */
void insertionSort(food_properties data[], int num_of_data, char to_sort[]) {
  food_properties stash; /* crate stash */
  int i, j;              /* iterator */

  if (strcmp(to_sort, "kode") == 0) { /* sort based on food_code_t */
    for (i = 0; i < num_of_data; i += 1) {
      stash = data[i];
      j = i - 1;
      while (strcmp(stash.food_code_t, data[j].food_code_t) == -1 && j >= 0) {
        data[j + 1] = data[j];
        j -= 1;
      }
      data[j + 1] = stash;
    }
  } else if (strcmp(to_sort, "nama") == 0) { /* sort based on food_name_t */
    for (i = 0; i < num_of_data; i += 1) {
      stash = data[i];
      j = i - 1;
      while (strcmp(stash.food_name_t, data[j].food_name_t) == -1 && j >= 0) {
        data[j + 1] = data[j];
        j -= 1;
      }
      data[j + 1] = stash;
    }
  } else if (strcmp(to_sort, "harga") == 0) { /* sort based on food_price_t */
    for (i = 0; i < num_of_data; i += 1) {
      stash = data[i];
      j = i - 1;
      while ((stash.food_price_t < data[j].food_price_t) && j >= 0) {
        data[j + 1] = data[j];
        j -= 1;
      }
      data[j + 1] = stash;
    }
  }
}

/**
 * binarySearchString, search for data with binary search method. this function
 * used specifically for char datatype.
 * @param num_of_data how much data to search?
 * @param data        array of data to search.
 * @param to_search   what kind of item we're gonna search for?
 * @param search_for  what to search?
 *
 * TODO: search item with only one function regardless of it's datatype
 */
void binarySearchString(int num_of_data,
                        food_properties data[],
                        char to_search[],
                        char search_for[]) {
  int i = 0;
  int j = num_of_data - 1;
  int k;
  int not_found = 1;

  if (strcmp(to_search, "kode") == 0) { /* search from food_code_t */
    while (not_found && (i <= j)) {
      k = (i + j) / 2;
      if (strcmp(data[k].food_code_t, search_for) == 0) {
        not_found = 0;
      } else if (strcmp(data[k].food_code_t, search_for) == -1) {
        i = k + 1;
      } else {
        j = k - 1;
      }
    }
  } else if (strcmp(to_search, "nama") == 0) { /* search from food_name_t */
    while (not_found && (i <= j)) {
      k = (i + j) / 2;
      if (strcmp(data[k].food_name_t, search_for) == 0) {
        not_found = 0;
      } else if (strcmp(data[k].food_name_t, search_for) == -1) {
        i = k + 1;
      } else {
        j = k - 1;
      }
    }
  }

  // output
  if (not_found) {
    printf("tidak ada\n");
  } else {
    printf("%s %s %d\n", data[k].food_code_t, data[k].food_name_t,
           data[k].food_price_t);
  }
}

/**
 * binarySearchString, search for data with binary search method. this function
 * used specifically for int datatype.
 * @param num_of_data how much data to search?
 * @param data        array of data to search.
 * @param to_search   what kind of item we're gonna search for?
 * @param search_for  what to search?
 *
 * TODO: search item with only one function regardless of it's datatype
 */
void binarySearchInt(int num_of_data,
                     food_properties data[],
                     char to_search[],
                     int search_for) {
  int i = 0;
  int j = num_of_data - 1;
  int k;
  int not_found = 1;

  if (strcmp(to_search, "harga") == 0) /* just to make sure */
  {
    while (not_found && (i <= j)) {
      k = (i + j) / 2;
      if (data[k].food_price_t == search_for) {
        not_found = 0;
      } else if (data[k].food_price_t < search_for) {
        i = k + 1;
      } else {
        j = k - 1;
      }
    }
  }

  // output
  if (not_found) {
    printf("tidak ada\n");
  } else {
    printf("%s %s %d\n", data[k].food_code_t, data[k].food_name_t,
           data[k].food_price_t);
  }
}