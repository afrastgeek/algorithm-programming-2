/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Masa Depan Alpro
 * 2. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan
 * saya bersedia menerima hukumanNya. Aamiin.
 */

/*
 * ----------------------------------------------------------------------------
 * Header File
 * ----------------------------------------------------------------------------
 * Here is where i register all needed libraries, global variable, and
 * function.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*---------------------------------------------------------------------------*/
/* Type and Structure declarations                                           */
/*---------------------------------------------------------------------------*/
struct axis {
  int x;
  int y;
};

struct item_count {
  int food;
  int travel;
};

typedef struct player_t {
  int energy;
  struct axis position;
} player_t;

typedef struct food_properties_t {
  struct axis position;
  char name[64];
  int protein;
} food_properties_t;

typedef struct travel_properties_t {
  struct axis position;
} travel_properties_t;

typedef struct property_max_length {
  struct axis position;
  int name;
  int protein;
} property_max_length;

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
food_properties_t food[256];
travel_properties_t movement[256];
property_max_length len;
player_t a;
struct item_count num_of;
char board[1024][1024];

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
int findFoodData(char to_find[15]);
int findPositionData(int x_to_find, int y_to_find);

/*---------------------------------------------------------------------------*/
/* Procedure prototypes                                                      */
/*---------------------------------------------------------------------------*/
void wait(float x);
void splashScreen();
void setMap(int width, int length);
void drawMap(int width, int length, int animate);
void readData(char selector[8]);
void findMaxLength(char selector[]);
void listData(char selector[]);
void insertData(char selector[], food_properties_t input);
void deleteData(char selector[], food_properties_t input);