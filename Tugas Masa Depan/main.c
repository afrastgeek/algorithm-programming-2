/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Masa Depan Alpro
 * 2. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan
 * saya bersedia menerima hukumanNya. Aamiin.
 */

/*
 * ------------------------------------------------------------------------------
 * Main Program File
 * ------------------------------------------------------------------------------
 * Here is where i build the program mainframe.
 *
 * TODO:
 * * Follow Coding Standard
 * * No Global Variables
 * * Optimization
 * * Watch for Memory Leak
 */

#include "header.h"

/**
 * @brief      Main program function.
 *
 * @param[in]  argc  Command line argument count.
 * @param[in]  argv  Command line argument variable.
 *
 * @return     Return an int as program running on hosted environment (on top
 *             of an OS) SEE ISO C 9899:1999.
 *
 * @details    This function are the main program function, which every local
 *             variable declaration, user input prompt, and pattern display are
 *             executed.
 */
int main(int argc, char const *argv[]) {
  unsigned int select_menu;
  unsigned int select_submenu;
  int in_deep = 0;
  int animate = 0;

  /**
   * @brief      Board Properties
   */
  struct board_properties {
    int width, length;
  } board_properties;

  food_properties_t input;

  board_properties.length = board_properties.width = 0;

  /**
   * @brief      Reads a data.
   *
   * @param[in]  <unnamed>  "both" as selector to read both from food
   *                        (tmakanan) and travel (tperjalanan) files.
   */
  readData("both");

  /**
   * Menu Loop
   */
  do {
    system("cls");
    /**
     * Check wether board has been set or not.
     */
    if (board_properties.width && board_properties.length) { /* if set */
      a.energy = 0;
      printf("\n");
      /**
       * @brief      Sets the map. Initialize map by filling each matrix
       *             element with empty space (" ").
       *
       * @param[in]  <unnamed>  board width property.
       * @param[in]  <unnamed>  board length property.
       */
      setMap(board_properties.width, board_properties.length);

      /**
       * @brief      Draws the map. Also animate the "A Cari Makan" when
       *             animate variable set at non-zero value
       *
       * @param[in]  <unnamed>  board width property.
       * @param[in]  <unnamed>  board length property.
       * @param[in]  <unnamed>  animate toggle.
       */
      drawMap(board_properties.width, board_properties.length, animate);
      animate = 0;
    } else { /*when board not yet set*/

      /**
       * @brief      Print splash screen. "A Cari Makan" title and author
       *             information.
       */
      splashScreen();
      printf(/*Notify user that map has not been set.*/
             "\e[4;5m"
             "\nPeta belum dibuat! Buat dengan menu 0.\n\n"
             "\e[0m");
    }

    /**
     * Main Menu
     *
     * @param[in]  <unnamed>  select_menu. Menu selector.
     */
    printf("Menu:\n");
    printf("0. Panjang dan Lebar Papan\n");
    printf("1. Kelola Makanan\n");
    printf("2. Kelola Perjalanan\n");
    printf("3. A Cari Makan\n");
    printf("4. Keluar\n");
    printf("Masukkan Menu: ");
    if (!in_deep) { /*if hasn't been in submenu*/
      scanf("%d", &select_menu);
    }
    printf("\n");

    switch (select_menu) {
      case 0: /*Set Board*/
        system("cls");
        printf("A Cari Makan > Kelola Perjalanan\n\n");
        printf("Masukkan Ukuran Papan.\n");
        printf("Ukuran papan saat ini: %d x %d (l x w)\n",
               board_properties.length, board_properties.width);
        printf("panjang/length/horizontal: ");
        scanf("%d", &board_properties.length);
        printf("lebar/width/vertical: ");
        scanf("%d", &board_properties.width);
        break;
      case 1: /*Manage Food*/
        system("cls");
        printf("A Cari Makan > Kelola Makanan\n\n");
        listData("food");

        printf("Menu:\n");
        printf("   1. Tambah\n");
        printf("   2. Hapus\n");
        printf("   3. Menu Utama\n");
        printf(" Pilihan Menu: ");
        scanf("%d", &select_submenu);
        printf("\n");
        switch (select_submenu) {
          case 1: /*Add Food*/
            printf("Tambah Makanan.\n");
            printf("x\t: ");
            scanf("%d", &input.position.x);
            printf("y\t: ");
            scanf("%d", &input.position.y);
            printf("nama\t: ");
            scanf("%s", input.name);
            printf("energi\t: ");
            scanf("%d", &input.protein);
            printf("\n");
            insertData("food", input);
            in_deep = 1;
            wait(2);
            break;
          case 2: /*Delete Food*/
            in_deep = 1;
            printf("Hapus Makanan.\n");
            printf("nama\t: ");
            scanf("%s", input.name);
            printf("\n");
            deleteData("food", input);
            break;
          case 3: /*Go to main menu*/
            in_deep = 0;
            break;
          default: /*Default response.*/
            in_deep = 1;
            printf("Menu yang dipilih tidak ada.\n");
            wait(3 / 2);
            printf("Silahkan pilih menu kembali.\n");
            wait(7 / 4);
            break;
        }
        break;
      case 2: /*Manage Travel*/
        system("cls");
        printf("A Cari Makan > Kelola Perjalanan\n\n");

        listData("travel");

        printf("Menu:\n");
        printf("   1. Tambah\n");
        printf("   2. Hapus\n");
        printf("   3. Menu Utama\n");
        printf(" Pilihan Menu: ");
        scanf("%d", &select_submenu);
        switch (select_submenu) {
          case 1: /*Add Travel move*/
            printf("Tambah Perjalanan.\n");
            printf("x\t: ");
            scanf("%d", &input.position.x);
            printf("y\t: ");
            scanf("%d", &input.position.y);
            insertData("travel", input);
            in_deep = 1;
            wait(2);
            break;
          case 2: /*Delete travel move*/
            in_deep = 1;
            printf("Hapus Langkah Perjalanan.\n");
            printf("x\t: ");
            scanf("%d", &input.position.x);
            printf("y\t: ");
            scanf("%d", &input.position.y);
            printf("\n");
            deleteData("travel", input);
            break;
          case 3: /*Go to Main Menu*/
            in_deep = 0;
            break;
          default: /*Default response*/
            in_deep = 1;
            printf("Menu yang dipilih tidak ada.\n");
            wait(3 / 2);
            printf("Silahkan pilih menu kembali.\n");
            wait(7 / 4);
            break;
        }
        break;
      case 3: /*Animate! A Cari Makan.*/
        animate = 1;
        break;
      case 4: /*Exit*/
        system("exit");
        break;
      default: /*Default response*/
        printf("Menu yang dipilih tidak ada.\n");
        wait(2);
    }
  } while (select_menu != 4);

  return 0;
}

/**
 * Thanks:
 *
 * @TOOLS
 *
 * Sublime Text (Sublime Text: The text editor you'll fall in love with),
 * https://www.sublimetext.com/
 *
 * SublimeLinter (SublimeLinter/SublimeLinter3),
 * https://github.com/SublimeLinter/SublimeLinter3
 *
 * nirm03 (nirm03/SublimeLinter-clang),
 * https://github.com/nirm03/SublimeLinter-clang
 *
 * LLVM (Clang Format), http://clang.llvm.org/
 *
 * rosshemsley (rosshemsley/SublimeClangFormat: A C++ code formatter based on
 * Clang Format, for beautiful code with minimal effort in Sublime Text 3),
 * https://github.com/rosshemsley/SublimeClangFormat/
 *
 * 20tauri (DoxyDoxygen), http://20tauri.free.fr/DoxyDoxygen/
 *
 * FIGlet and patorjk (Text to ASCII Art Generator),
 * http://patorjk.com/software/taag/#f=Small%20Slant&t=A%20Cari%20Makan%0A
 *
 * ----------------------------------------------------------------------------
 * @CODE STYLE & GUIDE
 *
 * Darren G. Moss (Efficient C/C++ Coding Techniques),
 * http://www.open-std.org/jtc1/sc22/wg21/docs/ESC_Boston_01_304_paper.pdf
 *
 * GNU (GNU coding standard), http://www.gnu.org/prep/standards/
 *
 * L.W. Cannon, et al. (Recommended C Style and Coding Standards),
 * https://www.doc.ic.ac.uk/lab/cplus/cstyle.html#N10081
 *
 * GCC (printf(3) - Linux man page), http://linux.die.net/man/3/printf
 *
 * cplusplus.com (C++ Language - C++ Tutorials),
 * http://www.cplusplus.com/doc/tutorial/
 *
 * ----------------------------------------------------------------------------
 * @ALGORITHM AND OTHERS
 *
 * Quoyo, OcuS, WhozCraig (Why is a negative int greater than unsigned int?),
 * http://stackoverflow.com/questions/13600991/
 *
 * admin on Pacific Simplicity (Colored printf messages in Linux),
 * https://www.pacificsimplicity.ca/blog/colored-printf-messages-linux
 *
 * rjm1 (Formatting Your Output),
 * https://www.le.ac.uk/users/rjm1/cotter/page_31.htm
 *
 * Google (Rectangle Area), https://www.google.co.id/search?q=rectangle+area
 *
 * Daniel D C, Null, MByD, Donnell, Igor (Passing struct to function),
 * http://stackoverflow.com/questions/10370047/
 *
 * Hideous Humpback Freak (Recursion – Good, Bad, or Indifferent?),
 * http://www.codeproject.com/Articles/1004806/Recursion-Good-Bad-or-Indifferent
 */
