/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Masa Depan Alpro
 * 2. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan
 * saya bersedia menerima hukumanNya. Aamiin.
 */

/*
 * ----------------------------------------------------------------------------
 * Program File
 * ----------------------------------------------------------------------------
 * Here is where i define all function and procedure.
 */

#include "header.h"

/**
 * @brief      Menjalankan animasi, menahan tampilan selama beberapa detik.
 *
 * @param[in]  x     Time to wait.
 */
void wait(float x) {
  time_t start;
  time_t current;
  time(&start);
  do
    time(&current);
  while (difftime(current, start) < x);
}

/**
 * @brief      Splash message.
 */
void splashScreen() {
  printf(/* A Cari Makan */
         "\t   ___     _____         _   __  ___     __           \n"
         "\t  / _ |   / ___/__ _____(_) /  |/  /__ _/ /_____ ____ \n"
         "\t / __ |  / /__/ _ `/ __/ / / /|_/ / _ `/  '_/ _ `/ _ \\\n"
         "\t/_/ |_|  \\___/\\_,_/_/ /_/ /_/  /_/\\_,_/_/\\_\\\\_,_/_//_/\n");
  printf(/* Author information */
         "\e[30;43m"
         "%79s\n%79s\n%79s\n%79s\n"
         "\e[0m",
         "", "M Ammar Fadhlur Rahman ", "1507506 - Kom-2C1 ",
         "2016, TMD Alpro II ");
}

/**
 * @brief      Set a map.
 *
 * @param[in]  width   The map width
 * @param[in]  length  The map length
 */
void setMap(int width, int length) {
  int row, col;
  int item;
  int letter;

  for (row = 0; row <= width; row += 1) {
    for (col = 0; col <= length; col += 1) {
      board[row][col] = ' ';
    }
  }
  for (row = 0; row <= width; row += 1) {
    for (col = 0; col <= length; col += 1) {
      for (item = 0; item < num_of.food; item += 1) {
        if (food[item].position.y == row && food[item].position.x == col) {
          for (letter = 0; letter < strlen(food[item].name); letter += 1) {
            board[row][col + letter] = food[item].name[letter];
          }
        }
      }
    }
  }
}

/**
 * @brief      Draws a map.
 *
 * @param[in]  width   The map width
 * @param[in]  length  The map length
 */
void drawMap(int width, int length, int animate) {
  int row, col;
  int item = 0;
  int dish;
  int letter;

  if (animate) {
    for (item = 0; item < num_of.travel; item += 1) {
      system("cls");
      for (row = -1; row < width + 2; row += 1) {
        printf("|");
        if (row == -1 || row == width + 1) {
          for (col = 0; col <= length; col += 1) {
            printf("-");
          }
        } else {
          for (col = 0; col <= length; col += 1) {
            if (movement[item].position.y == row &&
                movement[item].position.x == col) {
              printf(
                  "\e[30;5;43m"
                  "A"
                  "\e[0m");
              for (dish = 0; dish < num_of.food; dish += 1) {
                if (row == food[dish].position.y &&
                    col == food[dish].position.x) {
                  a.energy += food[dish].protein;
                  for (letter = 0; letter < strlen(food[dish].name);
                       letter += 1) {
                    board[row][col + letter] = ' ';
                  }
                }
              }
            } else {
              printf(
                  "\e[1;34;5;47m"
                  "%c"
                  "\e[0m",
                  board[row][col]);
            }
          }
        }
        printf("|\n");
      }
      printf(
          "\e[4m"
          "\nEnergi A: %d\n\n"
          "\e[0m",
          a.energy);
      wait(1);
    }
  } else {
    system("cls");
    for (row = -1; row < width + 2; row += 1) {
      printf("|");
      if (row == -1 || row == width + 1) {
        for (col = 0; col <= length; col += 1) {
          printf("-");
        }
      } else {
        for (col = 0; col <= length; col += 1) {
          if (movement[item].position.y == row &&
              movement[item].position.x == col) {
            printf(
                "\e[30;5;43m"
                "A"
                "\e[0m");
          } else {
            printf(
                "\e[1;34;5;47m"
                "%c"
                "\e[0m",
                board[row][col]);
          }
        }
      }
      printf("|\n");
    }
    printf(
        "\e[4m"
        "\nEnergi A: %d\n\n"
        "\e[0m",
        a.energy);
  }
}

/**
 * @brief      Reads the game data.
 *
 * @param[in]  selector  The selector to choose data to read. (food, travel, or
 *                       both)
 */
void readData(char selector[8]) {
  /**
   * @brief      Coordinate struct in character datatype.
   */
  struct stringified_coordinate {
    char x[8];
    char y[8];
  } s_coords;

  /*
   * @brief      Food property struct in character datatype.
   */
  struct stringified_food_property {
    struct stringified_coordinate coords;
    char name[64];
    char protein[8];
  } s_food_props;

  if (!strcmp(selector, "food") || !strcmp(selector, "both")) {
    FILE *fdata;
    fdata = fopen("data/tmakanan", "r");

    num_of.food = 0;
    do {
      fscanf(fdata, "%s %s %s %s", s_food_props.coords.x,
             s_food_props.coords.y, s_food_props.name, s_food_props.protein);
      if (strcmp(s_food_props.coords.x, "##") != 0 &&
          strcmp(s_food_props.coords.y, "##") != 0 &&
          strcmp(s_food_props.name, "##") != 0 &&
          strcmp(s_food_props.protein, "##") != 0) {
        food[num_of.food].position.x = atoi(s_food_props.coords.x);
        food[num_of.food].position.y = atoi(s_food_props.coords.y);
        strcpy(food[num_of.food].name, s_food_props.name);
        food[num_of.food].protein = atoi(s_food_props.protein);
        num_of.food += 1;
      }
    } while (strcmp(s_food_props.coords.x, "##") != 0 &&
             strcmp(s_food_props.coords.y, "##") != 0 &&
             strcmp(s_food_props.name, "##") != 0 &&
             strcmp(s_food_props.protein, "##") != 0);

    fclose(fdata);
  }
  if (!strcmp(selector, "travel") || !strcmp(selector, "both")) {
    FILE *fdata;
    fdata = fopen("data/tperjalanan", "r");

    num_of.travel = 0;
    do {
      fscanf(fdata, "%s %s", s_coords.x, s_coords.y);
      if (strcmp(s_coords.x, "##") != 0 && strcmp(s_coords.y, "##") != 0) {
        movement[num_of.travel].position.x = atoi(s_coords.x);
        movement[num_of.travel].position.y = atoi(s_coords.y);
        num_of.travel += 1;
      }
    } while (strcmp(s_coords.x, "##") != 0 && strcmp(s_coords.y, "##") != 0);
    fclose(fdata);
  }
}

/**
 * @brief      find character length of integer passed.
 *
 * @param[in]  n     interger to calculate.
 *
 * @return     return character length of the integer.
 */
int findIntLength(int n) {
  if (n > 10) {
    n /= 10;
    return 1 + findIntLength(n);
  } else if (n == 10) {
    return 2;
  } else {
    return 1;
  }
}

/**
 * @brief      find Max Length of each Properties declared.
 *
 * @param      selector  The selector, process based on this value.
 */
void findMaxLength(char selector[]) {
  int item;
  len.position.x = strlen("x");
  len.position.y = strlen("y");
  len.name = strlen("nama");
  len.protein = strlen("energi");

  if (strcmp(selector, "food") == 0) {
    for (item = 0; item < num_of.food; item += 1) {
      if (len.position.x < findIntLength(food[item].position.x)) {
        len.position.x = findIntLength(food[item].position.x);
      }
      if (len.position.y < findIntLength(food[item].position.y)) {
        len.position.y = findIntLength(food[item].position.y);
      }
      if (len.name < strlen(food[item].name)) {
        len.name = strlen(food[item].name);
      }
      if (len.protein < findIntLength(food[item].protein)) {
        len.protein = findIntLength(food[item].protein);
      }
    }
  } else if (strcmp(selector, "travel") == 0) {
    for (item = 0; item < num_of.travel; item += 1) {
      if (len.position.x < findIntLength(movement[item].position.x)) {
        len.position.x = findIntLength(movement[item].position.x);
      }
      if (len.position.y < findIntLength(movement[item].position.y)) {
        len.position.y = findIntLength(movement[item].position.y);
      }
    }
  }
}

/**
 * @brief      List selected data in table form.
 *
 * @param      selector  The selector, display data based on this.
 */
void listData(char selector[]) {
  int item;
  int total_width;
  int width_sum;
  char attribute[4][8] = {"x", "y", "nama", "energi"};

  if (!strcmp(selector, "food")) {
    readData(selector);
    findMaxLength(selector);
    width_sum = len.position.x + len.position.y + len.name + len.protein + 13;

    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n");
    printf("| %*s | %*s | %-*s | %*s |\n", len.position.x, attribute[0],
           len.position.y, attribute[1], len.name, attribute[2], len.protein,
           attribute[3]);
    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n");
    for (item = 0; item < num_of.food; item += 1) {
      printf("| %*d | %*d | %-*s | %*d |\n", len.position.x,
             food[item].position.x, len.position.y, food[item].position.y,
             len.name, food[item].name, len.protein, food[item].protein);
    }
    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n\n");
  } else if (!strcmp(selector, "travel")) {
    readData(selector);
    findMaxLength(selector);
    width_sum = len.position.x + len.position.y + 7;

    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n");
    printf("| %*s | %*s |\n", len.position.x, attribute[0], len.position.y,
           attribute[1]);
    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n");
    for (item = 0; item < num_of.travel; item += 1) {
      printf("| %*d | %*d |\n", len.position.x, movement[item].position.x,
             len.position.y, movement[item].position.y);
    }
    for (total_width = width_sum; total_width > 0; total_width--) printf("-");
    printf("\n\n");
  }
}

/**
 * @brief      Writes selected game data.
 *
 * @param      selector  The selector, write data based on this.
 */
void writeData(char selector[]) {
  int item;
  if (!strcmp(selector, "food")) {
    FILE *fdata;
    fdata = fopen("data/tmakanan", "w");
    for (item = 0; item < num_of.food; item += 1) {
      fprintf(fdata, "%d %d %s %d\n", food[item].position.x,
              food[item].position.y, food[item].name, food[item].protein);
    }
    fprintf(fdata, "## ## ## ##\n");
    fclose(fdata);
  } else if (!strcmp(selector, "travel")) {
    FILE *fdata;
    fdata = fopen("data/tperjalanan", "w");
    for (item = 0; item < num_of.travel; item += 1) {
      fprintf(fdata, "%d %d\n", movement[item].position.x,
              movement[item].position.y);
    }
    fprintf(fdata, "## ##\n");
    fclose(fdata);
  }
}

/**
 * @brief      Find Food Data from tmakanan file.
 *
 * @param      to_find  Data to find
 *
 * @return     Return 1 if data match.
 */
int findFoodData(char to_find[15]) {
  int status = 0;
  int item = 0;

  readData("food");
  while (!status && item < num_of.food) {
    if (strcmp(food[item].name, to_find) == 0) {
      status = 1;
    } else {
      item += 1;
    }
  }

  return status;
}

/**
 * @brief      Find Position Data from tperjalanan file.
 *
 * @param[in]  x_to_find  The x coordinates to find
 * @param[in]  y_to_find  The y coordinates to find
 *
 * @return     Return 1 if data match.
 */
int findTravelData(int x_to_find, int y_to_find) {
  int status = 0;
  int item = 0;

  readData("travel");
  while (!status && item < num_of.travel) {
    if (movement[item].position.x == x_to_find &&
        movement[item].position.y == y_to_find) {
      status = 1;
    } else {
      item += 1;
    }
  }

  return status;
}

/**
 * @brief      sort Food by it's Coordinate Location.
 */
void sortFoodByCoord() {
  food_properties_t insertion;
  int dish, item;

  for (dish = 0; dish < num_of.food; dish += 1) {
    insertion = food[dish];
    item = dish - 1;
    while (insertion.position.y < food[item].position.y && item >= 0) {
      food[item + 1] = food[item];
      item = item - 1;
    }
    food[item + 1] = insertion;
  }

  for (dish = 0; dish < num_of.food; dish += 1) {
    insertion = food[dish];
    item = dish - 1;
    while (insertion.position.x < food[dish].position.x &&
           insertion.position.y == food[dish].position.y && item >= 0) {
      food[item + 1] = food[item];
      item = item - 1;
    }
    food[item + 1] = insertion;
  }
}
/**
 * @brief      sort Travel Movement by it's Coordinate Location.
 */
void sortTravelByCoord() {
  travel_properties_t insertion;
  int move, item;

  for (move = 0; move < num_of.travel; move += 1) {
    insertion = movement[move];
    item = move - 1;
    while (insertion.position.y < movement[item].position.y && item >= 0) {
      movement[item + 1] = movement[item];
      item = item - 1;
    }
    movement[item + 1] = insertion;
  }

  for (move = 0; move < num_of.travel; move += 1) {
    insertion = movement[move];
    item = move - 1;
    while (insertion.position.x < movement[move].position.x &&
           insertion.position.y == movement[move].position.y && item >= 0) {
      movement[item + 1] = movement[item];
      item = item - 1;
    }
    movement[item + 1] = insertion;
  }
}

/**
 * @brief      Insert Game Data to file according to selector.
 *
 * @param      selector  The selector, write data based on this.
 * @param[in]  input     The input data to write.
 */
void insertData(char selector[], food_properties_t input) {
  int status;

  if (!strcmp(selector, "food")) {
    status = findFoodData(input.name);

    if (status == 0) {
      food[num_of.food] = input;
      num_of.food += 1;

      sortFoodByCoord();
      writeData("food");
      printf(
          "Makanan \e[4m%s\e[0m dengan kandungan protein "
          "\e[4m%d\e[0m\nberhasil "
          "ditambahkan di koordinat \e[4m(%d,%d)\e[0m.\n",
          input.name, input.protein, input.position.x, input.position.y);
    } else {
      printf("Makanan \e[4m%s\e[0m sudah ada, data gagal ditambahkan.\n",
             input.name);
    }
  } else if (!strcmp(selector, "travel")) {
    status = findTravelData(input.position.x, input.position.y);

    if (status == 0) {
      movement[num_of.travel].position = input.position;
      num_of.travel += 1;

      sortTravelByCoord();
      writeData("travel");
      printf(
          "Langkah perjalanan berhasil ditambahkan di koordinat "
          "\e[4m(%d,%d)\e[0m.\n",
          input.position.x, input.position.y);
    } else {
      printf(
          "Langkah di koordinat \e[4m(%d,%d)\e[0m sudah ada, data gagal "
          "ditambahkan.\n",
          input.position.x, input.position.y);
    }
  }
}

/**
 * @brief      Delete selected data from file.
 *
 * @param      selector  The selector, delete data based on this.
 * @param[in]  input     The input, data information to delete.
 */
void deleteData(char selector[], food_properties_t input) {
  int item;
  int status = 0;
  char confirmation_command[8];

  if (!strcmp(selector, "food")) {
    readData("food");

    item = 0;
    while (item < num_of.food && status == 0) {
      if (strcmp(food[item].name, input.name) == 0) {
        status = 1;

        printf("Data ditemukan.\nYakin hapus data? (ya/tidak): ");

        scanf("%s", confirmation_command);
        printf("\n");
      } else {
        item += 1;
      }
    }
    if (strcmp(confirmation_command, "ya") == 0) {
      for (; item < num_of.food; item += 1) {
        food[item] = food[item + 1];
      }
      num_of.food -= 1;
      writeData("food");
    } else {
      printf("Perintah tidak diketahui\n");
      printf("Penghapusan dibatalkan.\n");
    }
  } else if (!strcmp(selector, "travel")) {
    readData("travel");

    item = 0;
    while (item < num_of.travel && status == 0) {
      if (movement[item].position.x == input.position.x &&
          movement[item].position.y == input.position.y) {
        status = 1;

        printf("Data ditemukan.\nYakin hapus data? (ya/tidak): ");

        scanf("%s", confirmation_command);
        printf("\n");
      } else {
        item += 1;
      }
    }
    if (strcmp(confirmation_command, "ya") == 0) {
      for (; item < num_of.travel; item += 1) {
        movement[item] = movement[item + 1];
      }
      num_of.travel -= 1;
      writeData("travel");
    } else {
      printf("Perintah tidak diketahui\n");
      printf("Penghapusan dibatalkan.\n");
    }
  }
  if (status == 0) {
    printf("Data tidak ditemukan\n");
  }
  wait(3);
}
