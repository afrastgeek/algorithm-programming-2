/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 7 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure.
|
*/

#include "header.h"

void readFile(char file_name[], scoring data[]) {
  int n = 0;
  scoring foo;
  FILE *rec;

  rec = fopen(file_name, "r");

  do {
    fscanf(rec, "%s %s %f %d", foo.nim_t, foo.name_t, &foo.score_t,
           &foo.course_num_t);
    foo.gpa_t = foo.score_t / foo.course_num_t;
    data[n] = foo;
    n += 1;
  } while (strcmp(foo.nim_t, "##") != 0);

  fclose(rec);
}

void quickSort(scoring data[], int first, int last) {
  int pivot, i, j;
  scoring temp;

  if (first < last) {
    pivot = first;
    i = first;
    j = last;

    while (i < j) {
      while (data[i].gpa_t < data[pivot].gpa_t && i < last) i++;
      while (data[j].gpa_t > data[pivot].gpa_t) j--;
      if (i < j) {
        temp = data[i];
        data[i] = data[j];
        data[j] = temp;
      }
    }

    temp = data[pivot];
    data[pivot] = data[j];
    data[j] = temp;
    quickSort(data, first, j - 1);
    quickSort(data, j + 1, last);
  }
}