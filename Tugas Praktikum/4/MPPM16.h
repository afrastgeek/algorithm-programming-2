/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 4 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register needed libraries, and function.
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief The pattern block properties.
 * @details This struct hold an array of 5 integer which will be filled by
 * lastRowSpace function.
 *
 */
typedef struct presension_data {/* Struct declaration */
  char id_number_t[32]; /* Struct definition, populate name of student */
  char name_t[16];      /* Struct definition, populate the student classes */
} presension_data;      /* Altering Struct with Typedef declaration */

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
int isPrintLog(char error_level[]);
int isNumber(char should_number[32]);
int mustSort(char sort_var[]);
void bubbleSortId(int num_of_valid, presension_data valid[]);
void quickSortNama(int awal, int tengah, presension_data data[]);
