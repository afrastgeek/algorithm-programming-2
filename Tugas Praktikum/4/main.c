/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 4 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "MPPM16.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  /**
   * @brief Variable Declaration & Data Input Section
   */
  int num_of_data;      /* how much data to be processed */
  char error_level[16]; /* store the error_level settings, yes or not */
  scanf("%d %s", &num_of_data, error_level);

  presension_data student[num_of_data], valid[num_of_data],
      trash[num_of_data]; /* student in number of num_of_data */
  int item;               /* iterator */

  for (item = 0; item < num_of_data; item += 1) { /* input student id & name */
    scanf(" %s %s", student[item].id_number_t, student[item].name_t);
  } /* for item < num_of_data */

  char sort_var[4]; /* store the variable sort setting, sort by id or name */
  scanf(" %s", sort_var);

  /**
   * @brief Operation Section
   * - check id value
   * - check sort_var, sort or fail
   */
  int num_of_valid = 0, num_of_trash = 0;
  for (item = 0; item < num_of_data; item += 1) {
    if (isNumber(student[item].id_number_t)) {
      valid[num_of_valid] = student[item];
      num_of_valid += 1;
    } else {
      trash[num_of_trash] = student[item];
      num_of_trash += 1;
    }
  }

  /**
   * @brief Output Section
   */
  if (mustSort(sort_var)) {
    printf("---Hasil Pengurutan---\n");
    if (num_of_valid) {
      if (mustSort(sort_var) == 1) {
        bubbleSortId(num_of_valid, valid);
      } else if (mustSort(sort_var) == 2) {
        quickSortNama(0, num_of_valid - 1, valid);
      }
      for (item = 0; item < num_of_valid; item += 1) {
        printf("%s %s\n", valid[item].id_number_t, valid[item].name_t);
      }
    } else {
      printf("Tidak ada data valid.\n");
    }

    if (isPrintLog(error_level)) {
      printf("---Daftar Error---\n");
      for (item = 0; item < num_of_trash; item += 1) {
        printf("%s - %s tidak valid.\n", trash[item].id_number_t,
               trash[item].name_t);
      }
    }
  } else {
    printf("---Data tidak dapat diproses---\n");
  }

  return 0; /* return 0 as the program succeed */
} /* main function */
