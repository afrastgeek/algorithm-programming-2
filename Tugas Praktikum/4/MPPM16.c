/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 4 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define the function (or procedure, whatever).
|
*/

#include "MPPM16.h"

/**
 * @brief Check the error_level setting
 *
 * @param error_level the setting.
 * @return equivalent value.
 */
int isPrintLog(char error_level[]) {
  if (strcmp(error_level, "ya") == 0) {
    return 1;
  } else {
    return 0;
  }
} /* isPrintLog */

/**
 * @brief is the string given all number?
 *
 * @param should_number the string to be checked
 * @return yes or no in binary.
 */
int isNumber(char should_number[32]) {
  int not_id = 1;

  if (strlen(should_number) <= 7) {
    int i = 0;
    while (i < 7 && not_id) {
      if (should_number[i] >= '0' && should_number[i] <= '9') {
        not_id = 1;
      } else {
        not_id = 0;
      }
      i += 1;
    }
  } else {
    not_id = 0;
  }

  if (not_id) {
    return 1;
  } else {
    return 0;
  }
} /* isNumber */

/**
 * @brief sort!
 *
 * @return 0 when not qualify.
 */
int mustSort(char sort_var[]) {
  if (strcmp(sort_var, "nim") == 0) {
    return 1;
  } else if (strcmp(sort_var, "nama") == 0) {
    return 2;
  } else {
    return 0;
  }
}

void bubbleSortId(int num_of_valid, presension_data valid[]) {
  /* bubble sort */
  int i, j;
  presension_data temp;
  int sorted;

  for (i = 1; i < num_of_valid; i += 1) {  // repeat bubble pass n-1 times
    sorted = 1;  // break when no changes in a pass => already sorted

    j = 0;
    while (j < num_of_valid - i && sorted) {
      if (strcmp(valid[j].id_number_t, valid[j + 1].id_number_t) ==
          1) {  // n-i comparisons
        temp = valid[j];
        valid[j] = valid[j + 1];
        valid[j + 1] = temp;
        sorted = 0;
      }
      j += 1;
    }
  }  // so no need for further bubble passes
}

void quickSortNama(int awal, int tengah, presension_data data[]) {
  int i, p;

  presension_data temp;
  presension_data temp2;
  presension_data poros;

  i = awal;
  p = tengah;
  poros = data[(awal + tengah) / 2];

  do {
    // pengecekkan jika data awal lebih besar dari tengah
    while (strcmp(data[i].name_t, poros.name_t) == -1 && i <= p) {
      i++;
    }
    while (strcmp(data[p].name_t, poros.name_t) == 1 && i <= p) {
      p--;
    }

    if (i < p) {  // ketika i < p menunjukkan bahwa nilai data ke i, lebih kecil
                  // dari data ke p. Maka dilakukanlah pergantian
      temp = data[i];
      temp2 = data[i];

      data[i] = data[p];
      data[i] = data[p];

      data[p] = temp;
      data[p] = temp2;

      i += 1;
      p -= 1;
    }
  } while (i < p);

  if ((p > awal) && (p < tengah)) {  // ketika p masih belum berada di kanan,
                                     // ulangi kembali langkah
    quickSortNama(awal, p, data);
  }

  if ((i > awal) && (i < tengah)) {  // sama
    quickSortNama(i, tengah, data);
  }
}