/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 1 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "DLOM14.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, and pattern display are executed.
 * 
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 * 
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[])
{
    /**
     * @brief Declaration Section
     */
    int x, y; /* The initial coordinate variable */
    int step; /* Number of step to be done */
    
    /* Define matrix with static macro size (see: HEADER FILE).
     * The second dimension got 1 more length for assigning NULL character at
     * the end of the strings. */
    char map[MATRIXSIZE][MATRIXSIZE + 1]; 

    /* Call initMatrix procedure.
     * This procedure fill the matrix with asterisks '*'. */
    initMatrix(map);

    /**
     * @brief Matrix Operation Section
     */
    /* prompt user to input initial coordinates and number of step */
    scanf("%d %d %d", &x, &y, &step);
    
    x -= 1; /* translate humanic coordinates to mecha coordinates. */
    y -= 1; /* translate humanic coordinates to mecha coordinates. */

    map[y][x] = 'x'; /* replace initial coordinates with an 'x' mark. */

    /* Call markMatrix procedure.
     * This procedure PROMPT THE USER TO INPUT necessary step, direction and
     * step length. Then mark the step with corresponding character. */
    markMatrix(x, y, step, map);

    /**
     * @brief Matrix Display Section
     */
    for (g_row = 0; g_row < MATRIXSIZE; g_row += 1) { /* in number of row : */
        printf("%s\n", map[g_row]); /* print the matrix as a string */
    } /*for MATRIXSIZE*/

    return 0; /* return 0 as the program succeed */
} /*main function*/