/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 1 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
| *i know you're distinguishing it :)
|
*/

#include "DLOM14.h"

/*--------------------------------------------------------------------------*/
/* PATTERN PROCEDURE DECLARATION                                            */
/*--------------------------------------------------------------------------*/
/**
 * @brief initialize the matrix value
 * @details This procedure will fill each matrix value with asterisk ('*').
 * 
 * @param map   The target matrix to initialize.
 */
void initMatrix(char map[MATRIXSIZE][MATRIXSIZE+1]) {
    for (g_row = 0; g_row < MATRIXSIZE; g_row += 1) { /*for each row and...*/
        for (g_column = 0; g_column < MATRIXSIZE; g_column += 1) { /*column*/
            map[g_row][g_column] = '*'; /* fill matrix value with asterisk */
        } /*for MATRIXSIZE (row)*/
        map[g_row][MATRIXSIZE] = '\0'; /* assign NULL character at the end */
    } /*for MATRIXSIZE (column)*/
} /*void initMatrix*/


/**
 * @brief mark the matrix!
 * @details This procedure PROMPT THE USER TO INPUT necessary step, direction
 * and step length. Then mark the step with corresponding character.
 * 
 * @param x     The x-axis coordinates.
 * @param y     The y-axis coordinates.
 * @param step  Number of step to be done.
 * @param map   The target matrix to mark.
 */
void markMatrix(int x, int y, int step, char map[MATRIXSIZE][MATRIXSIZE+1]) {
    int which; /* step selector. which step are we on right now? */
    int step_length; /* step length. how much do you step each turn? */

    char step_direction[8]; /* step direction. which direction are up to? */

    for (which = 0; which < step; which += 1) { /*for each step*/
        /* prompt user to input */
        scanf(" %s %d", step_direction, &step_length);/*direction and length*/

        if (strcmp(step_direction, "atas") == 0) {
            for (g_row = 0; g_row < step_length; g_row += 1) {/*for each row*/
                y -= 1; /*move the coordinate*/
                map[y][x] = '|'; /*replace the matrix value with pipe char*/
            } /*for step_length*/
        } /*if atas*/
        else if (strcmp(step_direction, "bawah") == 0) {
            for (g_row = 0; g_row < step_length; g_row += 1) {/*for each row*/
                y += 1; /*move the coordinate*/
                map[y][x] = '|'; /*replace the matrix value with pipe char*/
            } /*for step_length*/
        } /*if bawah*/
        else if (strcmp(step_direction, "kiri") == 0) {
            for (g_column = 0; g_column < step_length; g_column += 1) {
                x -= 1; /*move the coordinate*/
                map[y][x] = '-'; /*replace the matrix value with dash char*/
            } /*for step_length*/
        } /*if kiri*/
        else if (strcmp(step_direction, "kanan") == 0) {
            for (g_column = 0; g_column < step_length; g_column += 1) {
                x += 1; /*move the coordinate*/
                map[y][x] = '-'; /*replace the matrix value with dash char*/
            } /*for step_length*/
        } /*if kanan*/
    } /*for step*/
} /*void markMatrix*/