/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 1 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
| (and procedure, if you distinguish it from function ;).
|
*/

#include <stdio.h>
#include <string.h>


/*--------------------------------------------------------------------------*/
/* Variable declarations                                                    */
/*--------------------------------------------------------------------------*/
int g_row, g_column;    /* Pattern Loop Counter, used in MAIN and PROGRAM */


/*--------------------------------------------------------------------------*/
/* Macro declarations                                                       */
/*--------------------------------------------------------------------------*/
#define MATRIXSIZE (10)    /* Translate the MATRIXSIZE number */

/*--------------------------------------------------------------------------*/
/* Procedure prototypes                                                     */
/*--------------------------------------------------------------------------*/

void initMatrix(char map[MATRIXSIZE][MATRIXSIZE+1]);
void markMatrix(int x, int y, int step, char map[MATRIXSIZE][MATRIXSIZE+1]);