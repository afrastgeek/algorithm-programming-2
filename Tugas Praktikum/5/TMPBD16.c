/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 5 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "TMPBD16.h"

void input(bigdata data[], int num_of_data) {
  if (num_of_data > 50) num_of_data = 50;
  int i;
  for (i = 0; i < num_of_data; i += 1) {
    scanf("%s", data[i].binary);
  }
}

int convBinary(char bin[]) {
  int i;
  int power;
  int length = strlen(bin);
  int result;

  result = 0;
  power = 1;

  for (i = length - 1; i >= 0; i -= 1) {
    if (bin[i] == '1') {
      result += power;
    }
    power *= 2;
  }

  return result;
}

void searchCompare(int same_value[], bigdata a[], bigdata b[], bigdata c[],
                   int n, int m, int o) {
  int i, j, k;
  same_value[0] = 0;
  int l = 1;

  for (i = 0; i < n; i += 1) {
    for (j = 0; j < m; j += 1) {
      if (strcmp(a[i].binary, b[j].binary) == 0) {
        for (k = 0; k < o; k += 1) {
          if (strcmp(a[i].binary, c[k].binary) == 0) {
            same_value[l] = convBinary(a[i].binary);
            l += 1;
          }
        }
      }
    }
  }
  same_value[0] = l;
}

void mergeThisUnordered(bigdata result[], bigdata a[], bigdata b[], bigdata c[],
                        int n, int m, int o) {
  int i, l;
  l = 0;
  for (i = 0; i < n; ++i) {
    result[l] = a[i];
    l++;
  }
  for (i = 0; i < m; ++i) {
    result[l] = b[i];
    l++;
  }
  for (i = 0; i < o; ++i) {
    result[l] = c[i];
    l++;
  }
}

void quickSort(bigdata data[], int first, int last) {
  int pivot, j, i;
  bigdata temp;

  if (first < last) {
    pivot = first;
    i = first;
    j = last;

    while (i < j) {
      while (strcmp(data[i].binary, data[pivot].binary) <= 0 && i < last) i++;
      while (strcmp(data[j].binary, data[pivot].binary) == 1) j--;
      if (i < j) {
        temp = data[i];
        data[i] = data[j];
        data[j] = temp;
      }
    }

    temp = data[pivot];
    data[pivot] = data[j];
    data[j] = temp;
    quickSort(data, first, j - 1);
    quickSort(data, j + 1, last);
  }
}