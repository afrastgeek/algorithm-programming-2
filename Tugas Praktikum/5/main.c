/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 5 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "TMPBD16.h"

int main(int argc, char const *argv[]) {
  /*
    Input and Variable Declaration
  */
  int it;
  int n, m, o;     /* declare the number of data */
  int total_data;  /* declare the total data counted */
  scanf("%d", &n); /* request user to input number of data */
  bigdata tobi[n]; /* initialize variable to store data */
  input(tobi, n); /* run input procedure */ /* REPEAT! */

  scanf("%d", &m);
  bigdata moni[m];
  input(moni, m);

  scanf("%d", &o);
  bigdata piyu[o];
  input(piyu, o);
  /* Data Gathered! Tobi, Piyu, and Moni job's done. Leave out all the rest. */

  /*
    Data Processing
  */
  /* Merge */
  total_data = n + m + o;
  bigdata result[total_data];
  mergeThisUnordered(result, tobi, moni, piyu, n, m, o);

  /* Sort */
  quickSort(result, 0, total_data - 1);

  /* Search */
  int decimal[n];
  searchCompare(decimal, tobi, moni, piyu, n, m, o);
  
  /*
    Output
  */
  printf("Laporan Data Biner\n====================\n");
  for (it = 0; it < total_data; it += 1) {
    printf("%s\n", result[it].binary);
  }
  for (it = 1; it < decimal[0]; it += 1) {
    printf("%d\n", decimal[it]);
  }

  return 0;
}
