/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 5 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/
typedef struct { char binary[16]; } bigdata;

#include <stdio.h>
#include <string.h>

void input(bigdata data[], int num_of_data);
int convBinary(char bin[]);
void searchCompare(int same_value[], bigdata a[], bigdata b[], bigdata c[],
                   int n, int m, int o);
void mergeThisUnordered(bigdata result[], bigdata a[], bigdata b[], bigdata c[],
                        int n, int m, int o);
void quickSort(bigdata data[], int first, int last);