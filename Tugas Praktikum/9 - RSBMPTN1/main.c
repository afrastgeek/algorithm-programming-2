/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 9 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
| I try to make self commenting code, in which variable name describe itself.
|
*/

#include "RSBMPTN1.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  int num_of_registrant; /* num_of_registrant */
  int person;            /* person iterator */
  int is_initial;        /* initial word switch */
  char city[32];         /* store city data */
  int letter;

  scanf("%d", &num_of_registrant);      /* get number of registrant */
  char inquiry[num_of_registrant][256]; /* create inquiry form */
  int result[num_of_registrant];        /* store matched inquiry condition */

  for (person = 0; person < num_of_registrant; person += 1) {
    scanf(" %255[^\n]s", inquiry[person]); /* get inquiry */
    result[person] = 0;                    /* initialize matched condition */
    STARTKATA(inquiry[person]);            /* initialize word engine */
    is_initial = 1;                        /* Toggle initial word status */
    do {                                   /* do this, do that */
      if (is_initial && isAllUpper(GETCKATA())) { /* REG is uppercase */
        result[person] += 1;          /* increment matched condition */
        is_initial = 0;               /* Toggle initial word status */
        INCKATA(inquiry[person]);     /* Go to the next word */
        if (isAllUpper(GETCKATA())) { /* SBMPTN is uppercase */
          result[person] += 1;        /* increment matched condition */
          INCKATA(inquiry[person]);   /* Go to the next word */
          if (countChar(GETCKATA(), '_') >= 2) { /* Nama Peserta has 3 sect. */
            result[person] += 1;         /* increment matched condition */
            INCKATA(inquiry[person]);    /* Go to the next word */
            if (!isHasNum(GETCKATA())) { /* Nama Kota doesn't contain number */
              result[person] += 1;       /* increment matched condition */
              strcpy(city, GETCKATA());  /* copy city data to city variable */
              INCKATA(inquiry[person]);  /* Go to the next word */
              if (isAllUpper(GETCKATA())) { /*Nama Sekolah is uppercase and...*/
                result[person] += 1;        /* increment matched condition */
                if (countChar(GETCKATA(), '_') == 2) { /* has 3 section */
                  result[person] += 1; /* increment matched condition */
                  convToUpper(city);   /* convert Nama Kota to uppercase */
                  /* if City data matched Cities on school identity */
                  /* can't get strcmpi working on CSPC, use strcmp instead */
                  // if (!strcmpi(city, getString(GETCKATA(), '_', 2))) {
                  if (!strcmp(city, getString(GETCKATA(), '_', 2))) {
                    result[person] += 1;      /* increment matched condition */
                    INCKATA(inquiry[person]); /* Go to the next word */
/*  <(¬_¬<)       *//* if Kode got an asterisk on it */
/* o<(¬_¬<)       *//* increment matched condition */
/* (>0o0)>=======O*/if (countChar(GETCKATA(), '*')) result[person] += 1;
                  } /* matching city data */
                } /* Nama Sekolah has 3 section */
              } /* and all uppercase */
            } /* Nama Kota doesn't contain number */
          } /* Nama Peserta has 3 section */
        } /* SBMPTN is all uppercase */
      } /* REG is all uppercase */
      INCKATA(inquiry[person]); /* If doesn't match any condition, increment */
    } while (EOPKATA(inquiry[person]) == 0); /* while not facing period EOP */
  }

  for (person = 0; person < num_of_registrant; person += 1) {
    if (result[person] != 8) printf("tidak "); /* if not matched all cond. */
    printf("valid\n");
  }

  return 0; /* return 0 as the program succeed*/
} /* main function */
