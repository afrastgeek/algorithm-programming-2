/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 9 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Global variable declarations                                             */
/*--------------------------------------------------------------------------*/
/* WORD ENGINE DEPENDENCY */
int indeks;
int panjangkata;
char ckata[50];

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
/* WORD ENGINE FUNCTION */
void STARTKATA(char pita[]);
void INCKATA(char pita[]);
void ADVKATA(char pita[]);
char* GETCKATA();
int GETPANJANGKATA();
int EOPKATA();

/* CHARACTER CHECKER FUNCTION */
int isUpper(char d);
int isAllUpper(char d[]);
int countChar(char d[], char c);
int isNum(char d);
int isHasNum(char d[]);

/**
 * it's easier to use strtok(). but prohibited.
 * then i try to return a string, but they expect for casting string to integer.
 * i argue. ψ(｀∇´)ψ
 */
char* getString(char d[], char c, int a);

void convToUpper(char s[]);