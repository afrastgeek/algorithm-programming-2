/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 9 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "RSBMPTN1.h"

/*--------------------------------------------------------------------------*/
/* Word Engine                                                              */
/*--------------------------------------------------------------------------*/
/**
 * STARTKATA    Initialize Word Engine.
 * @description This function will initialize the index accessor of data.
 * @param pita  Data to use.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
void STARTKATA(char pita[]) {
  indeks = 0;
  panjangkata = 0;
  while (pita[indeks] == ' ') {
    indeks += 1;
  }
  while ((pita[indeks] != ' ') && (pita[indeks] != '.')) {
    ckata[panjangkata] = pita[indeks];
    indeks += 1;
    panjangkata += 1;
  }
  ckata[panjangkata] = '\0';
}

/**
 * RESETKATA    Reset the Word Engine.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
void RESETKATA() {
  panjangkata = 0;
  ckata[panjangkata] = '\0';
}

/**
 * INC          Increment the word from data.
 * @param pita  Data to use.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
void INCKATA(char pita[]) {
  panjangkata = 0;
  while (pita[indeks] == ' ') {
    indeks += 1;
  }
  while ((pita[indeks] != ' ') && (pita[indeks] != '.')) {
    ckata[panjangkata] = pita[indeks];
    indeks += 1;
    panjangkata += 1;
  }
  ckata[panjangkata] = '\0';
}

/**
 * GETCKATA   Get the current word from data.
 * @return    Return the word in which index currently on.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
char* GETCKATA() { return ckata; }

/**
 * GETPANJANGKATA   Get the current word length from data.
 * @return          Return integer of word length.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
int GETPANJANGKATA() { return panjangkata; }

/**
 * EOP          End-of-Process.
 * @description Word Engine will stop when the current end character is the
 *              limiter, in this case the limiter is period character ('.').
 * @return      integer value of 1 if current character met the condition.
 *
 * THIS FUNCTION USE GLOBAL VARIABLES.
 */
int EOPKATA(char pita[]) {
  if (pita[indeks] == '.') {
    return 1;
  } else {
    return 0;
  }
}

/**
 * isUpper    Check wether the character passed is in Uppercase form or not.
 * @param  d  Character to check.
 * @return    Integer value of 1 if d met the condition.
 */
int isUpper(char d) {
  if ((d >= 'A' && d <= 'Z') || d == '_' || (d >= '0' && d <= '9')) return 1;
  return 0;
}

/**
 * isAllUpper Check wether the string passed is in Uppercase form or not.
 * @param  d  String to check.
 * @return    Integer value of 1 if d met the condition.
 */
int isAllUpper(char d[]) {
  int length = strlen(d);
  int i;
  int counter = 0;
  for (i = 0; i < length; i += 1)
    if (isUpper(d[i])) counter += 1;
  if (counter == length) return 1;
  return 0;
}

/**
 * countChar  See how much selected character exist on string.
 * @param  d  String to check.
 * @param  c  Character to count.
 * @return    Integer value of number of counted character on string.
 */
int countChar(char d[], char c) {
  int length = strlen(d);
  int i;
  int counter = 0;
  for (i = 0; i < length; i += 1)
    if (d[i] == c) counter += 1;
  return counter;
}

/**
 * isNum      Check wether the character passed is a number or not.
 * @param  d  Character to check.
 * @return    Integer value of 1 if d met the condition.
 */
int isNum(char d) {
  if (d >= '0' && d <= '9') return 1;
  return 0;
}

/**
 * isHasNum   Check wether the string passed has a number in it or not.
 * @param  d  String to check.
 * @return    Integer value of 1 if d has a number.
 */
int isHasNum(char d[]) {
  int length = strlen(d);
  int i;
  int counter = 0;
  for (i = 0; i < length; i += 1)
    if (isNum(d[i])) counter += 1;
  if (counter) return 1;
  return 0;
}

/**
 * getString  Much like strtok() function. Get string after selected delimiter.
 * @param  d  Origin string to search from.
 * @param  c  Delimiter.
 * @param  a  Number of delimiter to skip.
 * @return    Return part string after selected delimiter.
 */
char* getString(char d[], char c, int a) {
  int length = strlen(d);
  static char result[32];
  int counter = 0;
  int i;
  int j = 0;
  for (i = 0; i < length; i += 1) {
    if (d[i] == c) counter += 1;
    if (counter == a && d[i] != c) {
      result[j] = d[i];
      j += 1;
    }
  }
  result[j] = '\0';
  return result;
}

/**
 * convToUpper  Convert passed string to uppercase.
 * @param s     string to convert.
 */
void convToUpper(char s[]) {
  int c = 0;
  while (s[c] != '\0') {
    if (s[c] >= 'a' && s[c] <= 'z') s[c] = s[c] - 32;
    c++;
  }
}
