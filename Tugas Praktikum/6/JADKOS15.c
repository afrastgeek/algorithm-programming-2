/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 6 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
*/

#include "JADKOS15.h"

/**
 * Search Function with Sequential Search Method.
 * @param  d Data to search from.
 * @param  s Start position. Later became Selector.
 * @param  e End position.
 * @param  f Find, data to find.
 * @return   Index location of data.
 */
int sequentialSearch(int d[], int s, int e, int f) {
  int nf = 1;               /* not found */
  while ((s < e) && (nf)) { /* Foreach data from s to e, and f still nf */
    if (d[s] == f) {        /* if data at s are f */
      nf = 0;               /* switch the nf */
      return s;             /* return s */
    }
    s += 1; /* otherwise, iterate s */
  }
  return -1; /* default value when data nf */
}

/**
 * Inverse Search Function with Sequential Search Method.
 * @param  d Data to search from.
 * @param  s Start position. Later became Selector.
 * @param  e End position.
 * @param  f Find, data not to find.
 * @return   Index location of data.
 */
int sequentialSearchInverse(int d[], int s, int e, int f) {
  int nf = 1;               /* not found */
  while ((s < e) && (nf)) { /* Foreach data from s to e, and f still nf */
    if (d[s] != f) {        /* if data at s are not f */
      nf = 0;               /* switch the nf */
      return s;             /* return s */
    }
    s += 1; /* otherwise, iterate s */
  }
  return -1; /* default value when data nf */
}
