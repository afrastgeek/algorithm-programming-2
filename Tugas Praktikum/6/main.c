/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 6 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
| I try to make self commenting code, in which variable name describe itself.
|
*/

#include "JADKOS15.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const *argv[]) {
  /*
    Variable Declaration
  */
  char workday_name[8][7] = {"Senin", "Selasa", "Rabu",  "Kamis",
                             "Jumat", "Sabtu",  "Minggu"};

  int people = 3;
  int act_start = 7; /* activity start hour */
  int act_end = 18;  /* activity end hour */
  int h, i, p;       /* iterator: hour, item, people */
  int day, start_hour, end_hour;
  int num_of_schedule;
  int isnt_found = 1;

  daily timespace[WORKDAY];

  for (i = 0; i < WORKDAY; i += 1) { /* for each work day */
    for (h = 0; h < 24; h += 1) timespace[i].hour_t[h] = 0; /* init hour to 0 */
  }

  /*
    Input Section
  */
  for (p = 0; p < people; p += 1) {
    scanf("%d", &num_of_schedule);
    for (i = 0; i < num_of_schedule; i += 1) {
      scanf("%d %d %d", &day, &start_hour, &end_hour);
      day -= 1;
      for (h = act_start; h < act_end; h += 1) {
        if (h >= start_hour && h < end_hour) timespace[day].hour_t[h] += 1;
      }
    }
  }

  /*
    Print the Result!
  */
  for (i = 0; i < WORKDAY; i += 1) {
    start_hour = sequentialSearch(timespace[i].hour_t, act_start, act_end, 0);
    end_hour =
        sequentialSearchInverse(timespace[i].hour_t, start_hour, act_end, 0);
    if (end_hour == -1) end_hour = act_end;
    if (start_hour < act_end && start_hour != -1) {
      isnt_found = 0;
      printf("%s %d.00 - %d.00\n", workday_name[i], start_hour, end_hour);
    }
    // for (h = act_start; h < act_end; h += 1) {
    //   printf("%d", timespace[i].hour_t[h]);
    // }
    // printf("\n");
  }
  if (isnt_found) printf("Tidak ada irisan jadwal.\n");

  return 0; /* return 0 as the program succeed*/
} /* main function */

/*


(JADKOS15) Jadwal Kosong Bersama


Pembuat Soal: Asisten Pemrograman VI

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB

Tobi, Moni dan Piyu berada di kelas yang berbeda. Mereka merupakan anggota dari
salah satu Departemen BEM KEMAKOM. Saat ini mereka sedang menentukan waktu rapat
rutin. Agar semua anggota dapat menghadiri rapat, yang dipilih adalah waktu
dimana semua anggota tidak memiliki jadwal kuliah (irisan jadwal kosong).
Buatlah program untuk mencari irisan jadwal kosong mereka, jika waktu
beraktivitas ketiganya tidak kurang dari pukul 7 dan tidak lebih dari pukul 18.
Gunakan algoritma searching sequential ataupun binary search untuk menyelesaikan
permasalahan ini!

Format masukan (diulang sebanyak 3 kali, untuk Tobi, Moni, kemudian Piyu) :
n (jumlah daftar jam yang dipakai untuk kuliah, mulai hari Senin-Jumat)
daftar jadwal, terdiri dari : [nomor_hari] [jam_mulai] [jam_selesai]

Format keluaran :
[nama_hari] [batas_awal_jam_kosong - batas_akhir]
Contoh Masukan

5
1 7 18 (maksudnya hari Senin, mulai dari jam 7 hingga jam 18)
2 7 10 (hari Selasa mulai jam 7 hingga jam 10, kemudian...
2 16 18 ...dilanjutkan jam 16 hingga jam 18. Berarti di hari selasa, Tobi punya
jadwal kosong dari pukul 10 s.d. pukul 16)
3 7 12  (dst.)
4 9 12

3
2 7 18
3 10 16
4 7 16

4
1 8 12
1 16 17
3 8 12
4 7 10

Contoh Keluaran

Rabu 16.00 - 18.00
Kamis 16.00 - 18.00
Jumat 7.00 - 18.00
Contoh Masukan 2

5
1 7 18
2 7 16
3 9 16
4 7 12
4 16 18

4
2 10 18
3 7 10
4 7 16
5 10 18

3
3 16 18
5 7 10
5 13 16

Contoh Keluaran 2

Tidak ada irisan jadwal.


 */