/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II dalam
 *  mengerjakan TP 6 Alpro II, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, and function
|
*/

#include <stdio.h>

/*
    DEFINE WORKDAY NUMBER
*/
#ifndef WORKDAY
#define WORKDAY 5
#endif

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief Daily schedule holder.
 * @details Create anonymous struct which hold schedule properties.
 * Then define this anonymous struct with "daily" typedef.
 */
typedef struct {
  int hour_t[24];
  int first_empty_t;
  int last_empty_t;
} daily;

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
int sequentialSearch(int d[], int s, int e, int f);
int sequentialSearchInverse(int d[], int s, int e, int f);