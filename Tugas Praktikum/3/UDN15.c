/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 3 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define the function (or procedure, whatever).
|
*/

#include "UDN15.h"

/**
 * @brief Sort report data by it's name, only when the score are equal.
 * @details This function will sort the student data based on student name
 * but only when the score are the same, in descending order.
 *
 * @param num_of_data how much data to be sorted?
 * @param student the structed data to be sorted.
 * @return sorted student data.
 */
void nameSortDesc(int num_of_data, report_data student[]) {
  report_data insertion;
  int i, j;

  // proses pengurutan
  for (i = 0; i < num_of_data; i += 1) {
    insertion = student[i];
    j = i - 1;
    while (strcmp(insertion.name_t, student[j].name_t) == 1
      && insertion.score_t == student[j].score_t &&j >= 0) {
      student[j + 1] = student[j];
      j = j - 1;
    }
    student[j + 1] = insertion;
  }
} /* nameSortDesc */


/**
 * @brief Sort report data by it's score and it's name
 * @details This function will sort the student data based on student score in
 * descending order. Then call nameSortDesc function (i insist, procedure are
 * function) to sort the student data based on student name but only when the
 * score are the same, also in descending order.
 *
 * @param num_of_data how much data to be sorted?
 * @param student the structed data to be sorted.
 * @return sorted student data.
 */
void reportSortDesc(int num_of_data, report_data student[]) {
  report_data insertion;
  int i, j;

  // proses pengurutan
  for (i = 0; i < num_of_data; i += 1) {
    insertion = student[i];
    j = i - 1;
    while (insertion.score_t > student[j].score_t && j >= 0) {
      student[j + 1] = student[j];
      j = j - 1;
    }
    student[j + 1] = insertion;
  }

  nameSortDesc(num_of_data, student);
} /* reportSortDesc */
