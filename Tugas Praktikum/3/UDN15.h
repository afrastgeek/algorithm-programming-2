/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 3 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register needed libraries, and function.
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief The pattern block properties.
 * @details This struct hold an array of 5 integer which will be filled by
 * lastRowSpace function.
 * 
 */
typedef struct report_data {    /* Struct declaration */
  char name_t[32]; /* Struct definition, populate name of student */
  char the_class_t[16]; /* Struct definition, populate the student classes */
  int score_t; /* Struct definition, populate the student score */
} report_data;  /* Altering Struct with Typedef declaration */

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
void reportSortDesc(int num_of_data, report_data student[]);
void nameSortDesc(int num_of_data, report_data student[]);
