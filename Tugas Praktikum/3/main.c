/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 3 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "UDN15.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  /**
   * @brief Declaration Section
   */
  int num_of_data;
  int item;

  scanf("%d", &num_of_data);

  report_data student[num_of_data];

  /**
   * @brief Input Section
   */
  for (item = 0; item < num_of_data; item += 1) {
    scanf(" %s %s %d", student[item].name_t, student[item].the_class_t, &student[item].score_t);
  }


  /**
   * @brief Operation Section
   */
  reportSortDesc(num_of_data, student);

  /**
   * @brief Output Section
   */
  for (item = 0; item < num_of_data; item += 1) {
    printf("%s %s %d\n", student[item].name_t, student[item].the_class_t, student[item].score_t);
  }
  

  return 0; /* return 0 as the program succeed */
} /* main function */
