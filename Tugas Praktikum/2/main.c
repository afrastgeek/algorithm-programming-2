/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 2 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "5KMMR16.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, function call and pattern display
 * are executed.
 *
 * @param argc  The parameter are additional. void also could be used.
 * @param argv  The parameter are additional. void also could be used.
 *
 * @return Based on ISO C 9899:1999, This function should return an integer
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const* argv[]) {
  /**
   * @brief Declaration Section
   */
  int row, column;  /* row and column accessor */
  int h_len, v_len; /* horizontal and vertical length of each matrix */
  scanf("%d %d", &h_len, &v_len); /* prompt user to input the length */

  int fibon_init[v_len][h_len];   /* init matrix, hold first number */
  int fibon_second[v_len][h_len]; /* second matrix, hold second number */
  int fibon_random[v_len][h_len]; /* this matrix hold the fibonacci number */
  int fibon_limit[v_len][h_len];  /* limit matrix, hold the desired limit */
  int fibon_result[v_len][h_len]; /* hold the result of position checking */

  /**
   * @brief Matrix Input Section
   * For each element at each row and column ...
   */
  for (row = 0; row < v_len; row += 1) {
    for (column = 0; column < h_len; column += 1) {
      scanf("%d", &fibon_init[row][column]); /* input the initial number */
    }                                        /* for v_len */
  }                                          /* for h_len */
  for (row = 0; row < v_len; row += 1) {
    for (column = 0; column < h_len; column += 1) {
      scanf("%d", &fibon_second[row][column]); /* input the next number */
    }                                          /* for v_len */
  }                                            /* for h_len */
  for (row = 0; row < v_len; row += 1) {
    for (column = 0; column < h_len; column += 1) {
      scanf("%d", &fibon_random[row][column]); /*input the desired fibon num*/
    }                                          /* for v_len */
  }                                            /* for h_len */

  /**
   * @brief Operation Section
   */
  for (row = 0; row < v_len; row += 1) {
    for (column = 0; column < h_len; column += 1) {
      scanf("%d", &fibon_limit[row][column]); /* input the limit */

      /**
       * @brief call fibonLocator function and store on fibon_result matrix.
       * @details This will call the fibonLocator function to get the
       * "desired number" position in fibonacci sets, which start from "init"
       * number followed by "second" number.
       */
      fibon_result[row][column] =
          fibonLocator(fibon_init[row][column], fibon_second[row][column],
                       fibon_random[row][column]);
    } /* for v_len */
  }   /* for h_len */

  /**
   * @brief Declaration Section
   */
  for (row = 0; row < v_len; row += 1) {
    for (column = 0; column < h_len; column += 1) {
      /**
       * @brief Checking the real position and print the responsible pattern
       * @details This section of code will checking each element of matrix,
       * wether the real position is bigger or smaller than the limit.
       * If bigger, print "O". If smaller, print "X".
       */
      if (fibon_result[row][column] < fibon_limit[row][column]) {
        printf("X");
      } else {
        printf("O");
      } /* if fibon_result < fibon_limit */
      if (column != h_len - 1) {
        printf(" ");
      } /* if column != h_len - 1 */
    }   /* for v_len */
    printf("\n");
  } /* for h_len */

  return 0; /* return 0 as the program succeed */
} /* main function */
