/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 2 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define the function.
|
*/

#include "5KMMR16.h"

/**
 * @brief Get the location of a fibonacci number (the threshold)
 * @details This function will add the first and the second argument, and call
 * itself when the second argument doesn't meet the threshold.
 *
 * @param first The starting position of fibonacci sets. lower number.
 * @param second The next number of "first" at fibonacci sets. bigger number.
 * @param threshold The target fibonacci number. Count the position till this.
 * @return Return the accumulated "return" a.k.a The Position.
 */
int fibonLocator(int first, int second, int threshold) {
  int mem; /* create temporary variable */

  if (second <= threshold) { /* when the "second" num still under threshold */
    mem = second;            /* store the "second" to temporary variable */
    second += first;         /* addition "second" with previous number */
    first = mem; /* replace the "first" with previous "second" value */
    /* return 1 (step marker) plus this function recursively */
    return 1 + fibonLocator(first, second, threshold);
  } else {    /* when the "second" num are pass the threshold */
    return 1; /* just return 1 */
  }           /* if second under threshold */
} /* fibonLocator */
