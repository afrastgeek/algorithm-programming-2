/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman II pada saat
 *  mengerjakan Tugas Praktikum 2 Algoritma dan Pemrograman II. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register needed libraries, and function.
|
*/

#include <stdio.h>

/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
int fibonLocator(int first, int second, int threshold);
